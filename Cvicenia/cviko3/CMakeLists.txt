cmake_minimum_required(VERSION 3.17)
project(cviko3)

set(CMAKE_CXX_STANDARD 14)

add_executable(cviko3 main.cpp)