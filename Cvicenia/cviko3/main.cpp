#include <iostream>
#include <string>
using namespace std;



class Auto{
    float m_najetoKm;
    float m_cenaZaDen;
    float m_vydelek;

public:
    Auto(float najetoKm, float cenaZaDen, float vydelaloKc){
        m_najetoKm = najetoKm;
        m_cenaZaDen = cenaZaDen;
        m_vydelek = vydelaloKc;
    }

    float getPujcovne(float pocetDnu){
        return m_cenaZaDen * pocetDnu;
    }

    void evidujZapujcku(float ujetoKm, float pocetDnu){
        m_najetoKm += ujetoKm;
        m_vydelek += getPujcovne(pocetDnu);
        upravPujcovne(ujetoKm);
    }

    void printInfo(){
        cout << "Auto ma najeto " << m_najetoKm << ", cena za den je: "
        << m_cenaZaDen << ", vydelalo: " << m_vydelek << endl;
    }

private:
    void upravPujcovne(float najetoKM){
        if (najetoKM > 10000){
            m_cenaZaDen -= m_cenaZaDen / 10;
        }
    }
};
int main() {
    Auto *ford = new Auto(10000, 500, 0);
    ford->evidujZapujcku(1000, 3);
    ford->printInfo();
    delete ford;
    return 0;
}
