//
// Created by Nick on 16. 11. 2020.
//
#include <iostream>
#include "Creature.h"

#ifndef CVIKO9_TILE_H
#define CVIKO9_TILE_H


class Tile {
protected:
    Creature* m_creature;
public:
    Tile(int creatureLeve);
    virtual void print() = 0;
    virtual ~Tile();

protected:
    void printCreature(std::string tileSymbol);
};


#endif //CVIKO9_TILE_H
