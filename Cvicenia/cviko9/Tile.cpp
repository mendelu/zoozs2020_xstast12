//
// Created by Nick on 16. 11. 2020.
//

#include "Tile.h"

Tile::Tile(int creatureLeve){
    if(creatureLeve == 0){
        m_creature = nullptr;
    }else{
        m_creature = new Creature(creatureLeve);
    }
}

Tile::~Tile() {
        delete m_creature;

}

void Tile::printCreature(std::string tileSymbol){
    if (m_creature != nullptr){
        std::cout << 'C' << m_creature->getLevel();
    }else {
        std::cout << tileSymbol;
    }
}