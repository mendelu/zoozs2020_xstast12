//
// Created by Nick on 16. 11. 2020.
//

#include "Tile.h"


#ifndef CVIKO9_FORESTTILE_H
#define CVIKO9_FORESTTILE_H


class ForestTile:public Tile {

public:
    ForestTile(int creatureLevel);
    void print();
};


#endif //CVIKO9_FORESTTILE_H
