//
// Created by Nick on 16. 11. 2020.
//
#include "Tile.h"
#include "ForestTile.h"
#include "SeaTile.h"
#include <vector>

#ifndef CVIKO9_MAP_H
#define CVIKO9_MAP_H


class Map {
    std::vector<std::vector<Tile*>> m_tiles;
    void createSmallMap();

public:
    Map(int size);
    void print();
};


#endif //CVIKO9_MAP_H
