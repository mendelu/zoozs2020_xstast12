

#ifndef CVIKO9_CREATURE_H
#define CVIKO9_CREATURE_H


class Creature {
    int m_level;
public:
    Creature(int level);
    int getLevel();
};


#endif //CVIKO9_CREATURE_H
