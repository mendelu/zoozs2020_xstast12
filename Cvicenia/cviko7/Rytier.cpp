//
// Created by Nick on 2. 11. 2020.
//

#include "Rytier.h"
#include <iostream>


Rytier::Rytier(int zivot, int utok, int obrana, Zbran *zbran, Brneni *brneni) {
    m_zivot = zivot;
    m_utok = utok;
    m_obrana = obrana;
    m_zbran = zbran;
    m_brnenie = brneni;
}


void Rytier::zoberNapoj(Napoj* napoj){
    m_napoje.push_back(napoj);
}
void Rytier::vypiPoslednyNapoj(){
    if(!m_napoje.empty()){
        m_zivot += m_napoje.at(m_napoje.size() - 1)->getBonus();
        zahodPoslednyNapoj();
    }
}
void Rytier::zahodPoslednyNapoj(){
    if(!m_napoje.empty()){
        m_napoje.pop_back();
    }

}
void Rytier::printInfo(){
    std::cout << "Rytier zivot: " << m_zivot << "\nutok: " << m_utok << "\nobrana: "
    << m_obrana
    << "\nutok zbrane: " << m_zbran->getBonus() <<"\nobrana brnenia: "
    << m_brnenie->getBonus() << std::endl;
}
void Rytier::odectiZdravi(int zivoty){
    m_zivot -= zivoty;
}

void Rytier::zautoc(Drak *drak, int hlava){
    if (m_utok + m_zbran->getBonus() > drak->getObrana()){
        drak->odectiZdravi(hlava, m_utok + m_zbran->getBonus());
    }
    else{
        odectiZdravi(drak->getUtok(hlava));
    }
}
