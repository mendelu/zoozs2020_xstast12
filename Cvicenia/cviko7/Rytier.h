//
// Created by Nick on 2. 11. 2020.
//

#ifndef CVIKO7_RYTIER_H
#define CVIKO7_RYTIER_H

#include <vector>
#include "Brneni.h"
#include "Zbran.h"
#include "Napoj.h"
#include "Drak.h"

class Rytier {
    int m_zivot;
    int m_utok;
    int m_obrana;

    Zbran* m_zbran;
    Brneni* m_brnenie;
    std::vector<Napoj*> m_napoje;


public:
    Rytier(int zivot, int utok, int obrana, Zbran *zbran, Brneni* brneni);

    void zoberNapoj(Napoj* napoj);
    void vypiPoslednyNapoj();
    void zahodPoslednyNapoj();
    void printInfo();
    void odectiZdravi(int zivoty);
    void zautoc(Drak *drak, int hlava);
};


#endif //CVIKO7_RYTIER_H
