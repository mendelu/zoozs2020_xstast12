//
// Created by Nick on 2. 11. 2020.
//

#ifndef CVIKO7_ZBRAN_H
#define CVIKO7_ZBRAN_H


class Zbran {
    int m_bonusUtoku;
    float m_vaha;
public:
    Zbran(int bonus, float vaha);

    int getBonus(){
        return m_bonusUtoku;
    }

    float getVaha(){
        return m_vaha;
    }

};


#endif //CVIKO7_ZBRAN_H
