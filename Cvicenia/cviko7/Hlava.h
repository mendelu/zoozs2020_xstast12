//
// Created by Nick on 3. 11. 2020.
//

#ifndef CVIKO7_HLAVA_H
#define CVIKO7_HLAVA_H


class Hlava {
    int m_utok;
    int m_zivot;
public:
    Hlava(int utok, int zivot);
    int getUtok();
    int getZivot();
    void odectiZdravi(int zdravie);
};


#endif //CVIKO7_HLAVA_H
