//
// Created by Nick on 2. 11. 2020.
//

#ifndef CVIKO7_BRNENI_H
#define CVIKO7_BRNENI_H


class Brneni {
    int m_bonusObrany;
    float m_vaha;
public:
    Brneni(int bonus, float vaha);

    int getBonus(){
        return m_bonusObrany;
    }

    float getVaha(){
        return m_vaha;
    }

};


#endif //CVIKO7_BRNENI_H
