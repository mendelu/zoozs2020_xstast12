#include <iostream>

#include "Brneni.h"
#include "Zbran.h"
#include "Napoj.h"
#include "Rytier.h"
#include "Drak.h"






int main() {
    Brneni *zelezneBrnenie = new Brneni(30, 7);
    Zbran *mec = new Zbran(30, 5);
    Napoj *elixirZivota = new Napoj(30);



    Rytier *nikolas = new Rytier(100, 50, 70, mec, zelezneBrnenie);
    Drak *ferdinand = new Drak(10,50,100);

    nikolas->printInfo();
    nikolas->zautoc(ferdinand, 1);
    nikolas->printInfo();


    return 0;
}
