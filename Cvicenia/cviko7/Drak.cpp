//
// Created by Nick on 3. 11. 2020.
//

#include "Drak.h"

Drak::Drak(int utokPrvejHlavy, int utokDruhejHlavy, int obrana){
    m_hlavy[0] = new Hlava(utokPrvejHlavy, 30);
    m_hlavy[1] = new Hlava(utokDruhejHlavy, 20);
    m_obrana = obrana;

}
int Drak::getObrana(){
    return m_obrana;
}
void Drak::odectiZdravi(int hlava, int zdravie){
    m_hlavy[hlava]->odectiZdravi(zdravie);
}
int Drak::getUtok(int ktoraHlava){
    return m_hlavy[ktoraHlava]->getUtok();
}

Drak::~Drak(){
    for (int i = 0; i < 2; i++) {
        delete m_hlavy[i];
    }
}

