//
// Created by Nick on 3. 11. 2020.
//

#ifndef CVIKO7_DRAK_H
#define CVIKO7_DRAK_H
#include "Hlava.h"
#include <iostream>
#include <c++/4.8.3/array>

class Drak {
    std::array<Hlava *, 2> m_hlavy;
    int m_obrana;
public:
    Drak(int utokPrvejHlavy, int utokDruhejHlavy, int obrana);
    int getObrana();
    void odectiZdravi(int hlava, int zdravie);
    int getUtok(int ktoraHlava);

    ~Drak();
};


#endif //CVIKO7_DRAK_H
