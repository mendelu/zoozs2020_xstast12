//
// Created by Nick on 3. 11. 2020.
//

#include "Hlava.h"
Hlava::Hlava(int utok, int zivot){
    m_zivot = zivot;
    m_utok = utok;
}

int Hlava::getUtok(){
    return m_utok;
}
int Hlava::getZivot(){
    return m_zivot;
}
void Hlava::odectiZdravi(int zdravie){
    m_zivot -= zdravie;
}