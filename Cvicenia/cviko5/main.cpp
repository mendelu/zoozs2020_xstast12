#include <iostream>
using namespace std;




class Kurz{
    string m_nazov;
    int m_cena;
public:
    Kurz(string nazov, int cena){
        setNazov(nazov);
        m_cena = cena;
    }

    string getNazev(){
        return m_nazov;
    }

private:
    void setNazov(string nazov){
        if (nazov != ""){
            m_nazov = nazov;
        }else{
            cout << "Chybny nazov" << endl;
            m_nazov = "Neznamy";
        }
    }
};



class Student{
    string m_jmeno;
    int m_rokNarozeni;
    string m_bydliste;

public:
    Student(string jmeno, int rokNarozeni, string bydliste){
        setJmeno(jmeno);
        setBydliste(bydliste);
        m_rokNarozeni = rokNarozeni;

    }

    Student(string jmeno, int rokNarozeni):Student(jmeno, rokNarozeni, "Neznamo"){
    }

    string getJmeno(){
        return m_jmeno;
    }

private:
    void setJmeno(string jmeno){
        if(jmeno != ""){
            m_jmeno = jmeno;
        }
        else{
            cout << "Neplatne meno" << endl;
            m_jmeno = "Neni znamo";
        }
    }

    void setBydliste(string bydliste){
        if(bydliste != ""){
            m_bydliste = bydliste;
        }
        else{
            cout << "Neplatne bydliste" << endl;
            m_bydliste = "Neni znamo";
        }
    }
};

class Zapis{
    int m_znamka;
    Student* m_zapsanyStudent;
    Kurz* m_zapsanyKurz;
public:
    Zapis(Kurz* kurz, Student* student){
        setKurz(kurz);
        setStudent(student);
        m_znamka = 5;
    }


    void printInfo(){
        if(m_zapsanyStudent != nullptr){
            cout << "Student: " << m_zapsanyStudent->getJmeno() << endl;
        }
        if (m_zapsanyKurz != nullptr){
            cout << "Kurz: " << m_zapsanyKurz->getNazev() << endl;
        }
        cout << "Znamka: " << m_znamka << endl << endl;


    }

    void setZnamka(int znamka){
        if ((znamka > 5) || (znamka < 1)){
            cout << "Zapis: znamka interval od 1-5!";
        }else{
            m_znamka = znamka;
        }

    }

private:
    void setKurz(Kurz* kurz){
        if (kurz != nullptr){
            m_zapsanyKurz = kurz;
        }else{
            cout << "Zapis: Chybny pointer na kurz." << endl;
            m_zapsanyKurz = nullptr;
        }
    }


    void setStudent(Student* student){
        if (student != nullptr){
            m_zapsanyStudent = student;
        }else{
            cout << "Zapis: Chybny pointer na studenta." << endl;
            m_zapsanyStudent = nullptr;
        }
    }
};

int main() {

    Student *david = new Student("David", 1990, "Brno");
    Kurz *zoo = new Kurz("ZOO", 500);

    Zapis *davidDoZoo = new Zapis(zoo, david);
    davidDoZoo->setZnamka(1);
    davidDoZoo->printInfo();



    delete david;
    delete zoo;
    delete davidDoZoo;
    return 0;
}
