#include "Hrdina.h"

Hrdina::Hrdina(std::string jmeno, int sila, int obratnost, int odolnost, Povolani povolani){
    m_jmeno = jmeno;
    m_sila = sila;
    m_obratnost = obratnost;
    m_odolnost = odolnost;
    m_povolani = povolani;
}


Hrdina * Hrdina::createHrdina(std::string jmeno, Povolani povolani) {
    Hrdina* hrdina = nullptr;

    if (jmeno == ""){
        SpravcaChyb* spravce = SpravcaChyb::getSpravceChyb();
        spravce->pridajChybu(Chyba{"Hrdina", "createHrdina", "Prazdne jmeno"});
    }

    if (povolani == Povolani::zlodej){
        hrdina = new Hrdina(jmeno, 10, 35, 15, povolani);
        return hrdina;
    }
    if (povolani == Povolani::rytier){
        hrdina = new Hrdina(jmeno, 30, 15, 25, povolani);
        return hrdina;
    }
    if (povolani == Povolani::hranicar){
        hrdina = new Hrdina(jmeno, 10, 25, 15, povolani);

    }
    return hrdina;
}

std::string Hrdina::getPovolaniPopis() {
    if (m_povolani == Povolani::zlodej){
        return "zlodej";
    }
    if (m_povolani == Povolani::rytier){
        return "rytier";
    }
    if (m_povolani == Povolani::hranicar){
        return "hranicar";
    }
    return "chyba";
}

void Hrdina::printInfo() {
    std::cout << getPovolaniPopis() << " " << m_jmeno << std::endl;
    std::cout << "Sila: \t\t" << m_sila << std::endl;
    std::cout << "Obratnost: \t" << m_obratnost << std::endl;
    std::cout << "Odolnost: \t" << m_odolnost << std::endl;
}

Povolani Hrdina::getPovolani() {
    return Hrdina::m_povolani;
}
