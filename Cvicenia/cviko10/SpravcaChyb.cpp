#include "SpravcaChyb.h"

SpravcaChyb* SpravcaChyb::s_spravca = nullptr;

void SpravcaChyb::pridajChybu(Chyba chyba){
    m_chyba.push_back(chyba);
}
void SpravcaChyb::print(){
    for (auto chyba:m_chyba) {
        std::cout << "Trida: " << chyba.nazevTridy << "\n";
        std::cout << "Metoda: " << chyba.nazevMetody << "\n";
        std::cout << "Chyba: " << chyba.popis << std::endl;
    }
}

SpravcaChyb * SpravcaChyb::getSpravceChyb() {
    if(s_spravca == nullptr){
        s_spravca= new SpravcaChyb();
    }
    return s_spravca;
}

SpravcaChyb::SpravcaChyb(){}

SpravcaChyb::~SpravcaChyb() {}
