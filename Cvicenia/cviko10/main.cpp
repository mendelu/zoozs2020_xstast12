#include "SpravcaHrdinov.h"
int main() {







    Hrdina* karol = SpravcaHrdinov::createHrdina("Karel", Povolani::rytier);
    Hrdina* jano = SpravcaHrdinov::createHrdina("Jano", Povolani::hranicar);
    Hrdina* jozo = SpravcaHrdinov::createHrdina("Jozo", Povolani::hranicar);
    Hrdina* igor = SpravcaHrdinov::createHrdina("igor", Povolani::zlodej);

    std::vector<Hrdina*> vybrany = SpravcaHrdinov::getHrdinaByPovolanie(Povolani::hranicar);
    for (Hrdina* hrdina : vybrany) {
        hrdina->printInfo();
        std::cout << "\n--------------------\n";
    }

    return 0;
}
