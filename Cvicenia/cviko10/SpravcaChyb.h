#ifndef CVIKO10_SPRAVCACHYB_H
#define CVIKO10_SPRAVCACHYB_H
#include <iostream>
#include <vector>
#include "Chyba.h"


class SpravcaChyb {
    std::vector<Chyba> m_chyba;
    static SpravcaChyb* s_spravca;
    SpravcaChyb();
    ~SpravcaChyb();

public:
    void pridajChybu(Chyba chyba);
    void print();

    static SpravcaChyb* getSpravceChyb();
};


#endif //CVIKO10_SPRAVCACHYB_H
