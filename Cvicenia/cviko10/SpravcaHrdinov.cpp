//
// Created by Nick on 28. 11. 2020.
//

#include "SpravcaHrdinov.h"

int SpravcaHrdinov::s_pocetHrdinov = 0;
std::vector<Hrdina*> SpravcaHrdinov::s_hrdinovia = {};


std::vector<Hrdina*> SpravcaHrdinov::getHrdinaByPovolanie(Povolani povolani){
    std::vector<Hrdina*> vyhovujuciHrdinovia = {};
    for (Hrdina* hrdina: s_hrdinovia) {
        if (hrdina->getPovolani() == povolani){
            vyhovujuciHrdinovia.push_back(hrdina);
        }
    }
    return vyhovujuciHrdinovia;
}


Hrdina* SpravcaHrdinov::createHrdina(std::string jmeno, Povolani povolani){
    Hrdina* novyHrdina = Hrdina::createHrdina(jmeno, povolani);
    s_hrdinovia.push_back(novyHrdina);
    s_pocetHrdinov++;
    return novyHrdina;
}