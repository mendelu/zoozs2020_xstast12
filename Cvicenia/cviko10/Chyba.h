//
// Created by Nick on 23. 11. 2020.
//

#ifndef CVIKO10_CHYBA_H
#define CVIKO10_CHYBA_H
#include <iostream>

struct Chyba{
    std::string nazevTridy;
    std::string nazevMetody;
    std::string popis;
};


#endif //CVIKO10_CHYBA_H
