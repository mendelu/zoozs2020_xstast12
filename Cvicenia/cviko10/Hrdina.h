#ifndef CVIKO10_HRDINA_H
#define CVIKO10_HRDINA_H
#include <iostream>
#include "SpravcaChyb.h"

enum class Povolani{
    rytier, zlodej, hranicar
};

class Hrdina {
    std::string m_jmeno;
    int m_sila;
    int m_obratnost;
    int m_odolnost;
    Povolani m_povolani;

    Hrdina(std::string jmeno, int sila, int obratnost, int odolnost, Povolani povolani);
    std::string getPovolaniPopis();
public:
    Povolani getPovolani();
    static Hrdina* createHrdina(std::string jmeno, Povolani povolani);
    void printInfo();
};




#endif //CVIKO10_HRDINA_H
