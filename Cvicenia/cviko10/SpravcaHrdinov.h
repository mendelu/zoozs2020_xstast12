//
// Created by Nick on 28. 11. 2020.
//

#ifndef CVIKO10_SPRAVCAHRDINOV_H
#define CVIKO10_SPRAVCAHRDINOV_H

#include <vector>
#include <iostream>
#include "Hrdina.h"



class SpravcaHrdinov {
    static int s_pocetHrdinov;
    static std::vector<Hrdina*> s_hrdinovia;
public:
    static std::vector<Hrdina*> getHrdinaByPovolanie(Povolani povolani);
    static Hrdina* createHrdina(std::string jmeno, Povolani povolani);
};


#endif //CVIKO10_SPRAVCAHRDINOV_H
