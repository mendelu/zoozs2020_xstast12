//
// Created by Nick on 8. 12. 2020.
//

#include "Lod.h"

Lod::Lod() {
    m_naklad = new Naklad();
}

Lod::~Lod() {delete m_naklad;}

void Lod::ulozAkci(Akce *akce) {
    m_akce.push_back(akce);
}

void Lod::provedAkci() {
    vypisAkce();
    m_akce.at(nactiVstupUzivatele())->provedAkci(m_naklad);

}

void Lod::vypisAkce() {
    for (int i = 0; i < m_akce.size(); i++) {
        std::cout << m_akce.at(i)->getPopis() << " [" << i << "]\n";
    }
}

int Lod::nactiVstupUzivatele() {
    int vstupUzivatele = 0;
    std::cout << "Zadej volbu: ";
    std::cin >> vstupUzivatele;
    return vstupUzivatele;
};
