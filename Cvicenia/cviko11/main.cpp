#include <iostream>
#include "Lod.h"
#include "vypisNaklad.h"
#include "PridejNaklad.h"

int main() {
    Lod* spaceship = new Lod();
    spaceship->ulozAkci(new vypisNaklad());
    spaceship->ulozAkci(new PridejNaklad());


    spaceship->provedAkci();
    spaceship->provedAkci();
    return 0;
}
