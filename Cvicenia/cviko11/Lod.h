#ifndef CVIKO11_LOD_H
#define CVIKO11_LOD_H

#include <iostream>
#include <vector>

#include "Akce.h"
#include "Naklad.h"



class Lod {
    Naklad* m_naklad;
    std::vector<Akce*> m_akce;

public:
    Lod();
    void ulozAkci(Akce* akce);
    void provedAkci();
    void vypisAkce();
    int nactiVstupUzivatele();

    ~Lod();

};


#endif //CVIKO11_LOD_H
