//
// Created by Nick on 8. 12. 2020.
//

#ifndef CVIKO11_NAKLAD_H
#define CVIKO11_NAKLAD_H


#include <vector>
#include "Zbozi.h"

class Naklad {
    std::vector<Zbozi> m_zbozi;
public:
    Naklad() {};
    Zbozi getZbozi(int ktere);
    int getPocetZbozi();
    void pridejZbozi(Zbozi zbozi);
};


#endif //CVIKO11_NAKLAD_H
