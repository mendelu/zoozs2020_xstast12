#ifndef CVIKO11_AKCE_H
#define CVIKO11_AKCE_H

#include <iostream>
#include "Naklad.h"

class Akce {
    std::string m_popis;
public:
    Akce(std::string popis);
    std::string getPopis();

    virtual void provedAkci(Naklad* naklad) = 0;
};


#endif //CVIKO11_AKCE_H
