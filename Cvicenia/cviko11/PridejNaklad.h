//
// Created by Nick on 8. 12. 2020.
//

#ifndef CVIKO11_PRIDEJNAKLAD_H
#define CVIKO11_PRIDEJNAKLAD_H

#include "Akce.h"


class PridejNaklad: public Akce {
public:
    PridejNaklad();
    void provedAkci(Naklad *naklad);
};


#endif //CVIKO11_PRIDEJNAKLAD_H
