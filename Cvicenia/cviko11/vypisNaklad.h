#ifndef CVIKO11_VYPISNAKLAD_H
#define CVIKO11_VYPISNAKLAD_H

#include "Akce.h"
class vypisNaklad: public Akce {
public:
    vypisNaklad();
    void provedAkci(Naklad* naklad);
};


#endif //CVIKO11_VYPISNAKLAD_H
