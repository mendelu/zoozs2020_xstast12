//
// Created by Nick on 8. 12. 2020.
//

#include "PridejNaklad.h"

PridejNaklad::PridejNaklad():Akce("Pridaj naklad") {}

void PridejNaklad::provedAkci(Naklad *naklad) {
    std::string popis, destinace;
    int cena;
    float vaha;

    std::cout << "Zadaj cena:\n";
    std::cin >> cena;
    std::cout << "Zadaj vaha:\n";
    std::cin >> vaha;

    std::cout << "Zadaj nazov:";
    std::getline(std::cin.ignore(1), popis);
    std::cout << "Zadaj destinace:";
    std::getline(std::cin, destinace);
    naklad->pridejZbozi(Zbozi{popis, cena, vaha, destinace});
}
