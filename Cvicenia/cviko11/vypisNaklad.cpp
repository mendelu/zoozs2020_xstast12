#include "vypisNaklad.h"

void vypisNaklad::provedAkci(Naklad *naklad) {
    for (int i = 0; i < naklad->getPocetZbozi(); i++ ) {
        std::cout << "Zbozi c. " << i << " : " << naklad->getZbozi(i).popis << std::endl;
    }
}

vypisNaklad::vypisNaklad() : Akce("Vypis naklad") {}
