//
// Created by Nick on 9. 11. 2020.
//
#include <iostream>

#ifndef CVIKO8_KONTAJNER_H
#define CVIKO8_KONTAJNER_H


class Kontajner {
    std::string m_popis;

public:
    Kontajner(std:: string popis);
    void vypisObsah();

};


#endif //CVIKO8_KONTAJNER_H
