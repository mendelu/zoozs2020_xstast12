//
// Created by Nick on 9. 11. 2020.
//

#include "Sklad.h"
#include <iostream>

Sklad::Sklad(int pocetPater){
//    std::vector<Patro*> patra(pocetPater, new Patro());
//    m_patra = patra;


    for (int i = 0; i < pocetPater; i++) {
        m_patra.push_back(new Patro());
    }
}

Sklad::~Sklad(){
    for (auto & i : m_patra) {
        delete i;
    }
}
void Sklad::ulozKontajner(Kontajner* kontajner, int patro, int pozice){
    if ((patro >= 0 ) and (patro < m_patra.size())){
        m_patra.at(patro)->ulozKontajner(kontajner, pozice);
    } else{
        std::cout << "Sklad::ulozKontajner - Patro je od 0 az " << m_patra.size()-1 << std::endl;
    }

}
void Sklad::odoberKontajner(int patro, int pozice){
    if ((patro>= 0 ) and (patro < m_patra.size())){
        m_patra.at(patro)->odeberKontajner(pozice);
    } else{
        std::cout << "Sklad::odoberKontajner - Patro je od 0 az " << m_patra.size()-1 << std::endl;
    }

}
void Sklad::vypisObsahSkladu(){
    std::cout << "Obsah skladu:" << std::endl;
    for (int i = 0; i < m_patra.size(); i++) {
        std::cout << "Patro c." << i;
        std::cout << " :" << std::endl;
        m_patra.at(i)->vypisObsahPatra();
    }



//    for (auto i :m_patra) {
//        i->vypisObsahPatra();
//    }
}
