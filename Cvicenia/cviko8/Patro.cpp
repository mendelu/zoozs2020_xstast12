//
// Created by Nick on 9. 11. 2020.
//

#include "Patro.h"

Patro::Patro(){
    for (Kontajner* &pozice:m_pozice) {
        pozice = nullptr;
    }
}
void Patro::ulozKontajner(Kontajner* kontajner, int pozice){
    if((pozice >= 0) and (pozice < 10)){
        if(m_pozice[pozice] == nullptr){
            m_pozice[pozice] = kontajner;
        }
        else{
            std::cout << "Patro::ulozKontajner - Pozice " << pozice << " je obsazena." << std::endl;
        }
    } else{
        std::cout << "Patro::ulozKontajner - Pozice na intervalu 0 az 9." << std::endl;
    }
}

void Patro::odeberKontajner(int pozice){
    if((pozice >= 0) and (pozice < 10)) {
        if (m_pozice[pozice] != nullptr) {
            m_pozice[pozice] = nullptr;
        } else {
            std::cout << "Patro::ulozKontajner - Pozice " << pozice << " je volna." << std::endl;
        }
    }else{
        std::cout << "Patro::odeberKontajner - Pozice na intervalu 0 az 9." << std::endl;
    }
}
void Patro::vypisObsahPatra(){

    for (int i = 0; i < 10; i++) {
        if(m_pozice[i] != nullptr){
            std::cout << "Obsah patra na Pozici " << i << ": ";
            m_pozice[i]->vypisObsah();
            std::cout << std::endl;
        }
    }
}
