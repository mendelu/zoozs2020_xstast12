#include <iostream>
#include "Sklad.h"

int main() {
    Sklad* sklad01 = new Sklad(5);
    Kontajner *k01 = new Kontajner("auta");
    Kontajner *k02 = new Kontajner("iPady");
    Kontajner *k03 = new Kontajner("motorky");
    Kontajner *k04 = new Kontajner("Samsung");

    sklad01->ulozKontajner(k01, 0, 0);
//    sklad01->ulozKontajner(k02, 0, 0);
    sklad01->ulozKontajner(k03, 0, 1);
//    sklad01->ulozKontajner(k04, 0, 11);
//    sklad01->ulozKontajner(k04, 110, 10);

    sklad01->vypisObsahSkladu();


    delete k01;
    delete k02;
    delete k03;
    delete k04;
    delete sklad01;
    return 0;
}
