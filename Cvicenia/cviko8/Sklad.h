//
// Created by Nick on 9. 11. 2020.
//

#ifndef CVIKO8_SKLAD_H
#define CVIKO8_SKLAD_H

#include "Patro.h"
#include <vector>

class Sklad {
    std::vector<Patro*> m_patra;
public:
    Sklad(int pocetPater);
    ~Sklad();
    void ulozKontajner(Kontajner* kontajner, int patro, int pozice);
    void odoberKontajner(int patro, int pozice);
    void vypisObsahSkladu();
};


#endif //CVIKO8_SKLAD_H
