//
// Created by Nick on 9. 11. 2020.
//

#ifndef CVIKO8_PATRO_H
#define CVIKO8_PATRO_H
#include "Kontajner.h"


class Patro {
    Kontajner* m_pozice[10];
public:
    Patro();
    void ulozKontajner(Kontajner* kontajner, int pozice);
    void odeberKontajner(int pozice);
    void vypisObsahPatra();
};


#endif //CVIKO8_PATRO_H
