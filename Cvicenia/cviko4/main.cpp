#include <iostream>
using namespace std;


class ErrorLogger{
    static string s_log;
public:
    static void addLog(string log){
        s_log += log += "\n";
    }

    static void printLog(){
        cout << s_log << endl;
    }
};

class ElectricCar{
    float m_maxAh;
    float m_availableAh;
    float m_maxChargeing;

public:
    float getMaxAh(){
        return m_maxAh;
    }

    float getAvailableAh(){
        return m_availableAh;
    }

    float getVoltage(){
        return m_maxChargeing;
    }

    void charge(float value){
        if ((m_availableAh+value) < m_maxAh){
            m_availableAh += value;
        }
        else{
            m_availableAh = m_maxAh;
        }
    }


    ElectricCar(float maxAh, float available, float maxVoltage){
        if (maxAh < 0 || available < 0 || maxVoltage < 0){
            ErrorLogger::addLog("Neplatny udaj pri tvorbe auta");
        }
        m_maxAh = maxAh;
        m_availableAh = available;
        m_maxChargeing = maxVoltage;
    }


};



class PowerStation{
    float m_maxCurrent;
    float m_hourChargeAh;

public:

    float getMaxCrrent(){
        return m_maxCurrent;
    }

    float getHourCharge(){
        return m_hourChargeAh;
    }

    void chargeForHour(ElectricCar* car){
        if (car->getVoltage() < m_maxCurrent){
            car->charge((car->getVoltage()/m_maxCurrent)*m_hourChargeAh);
        }
        else{
            car->charge(m_hourChargeAh);
        }

    }

    PowerStation(float maxCurrent, float hourCharge){
        m_maxCurrent = maxCurrent;
        m_hourChargeAh = hourCharge;
    }
};



string ErrorLogger::s_log = "";

int main() {

    PowerStation* ps1 = new PowerStation(4, 4);
    ElectricCar* skoda = new ElectricCar(20, 10, 3 );

    ps1->chargeForHour(skoda);
    cout << skoda->getAvailableAh() << endl;

    ErrorLogger::printLog();

    delete ps1;
    delete skoda;

    return 0;
}
