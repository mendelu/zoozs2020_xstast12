#include "Map.h"

Map* Map::s_map = nullptr;

Map::Map() {
    createLocations();
    assignLocationPointers();
}

void Map::createLocations(){
    std::vector<Location*> row0(3, nullptr);
    row0.at(0) = new EdgeOfVillage("Edge of village");
    row0.at(1) = new BossLocation("Boss");
    row0.at(2) = new Village("Village");

    std::vector<Location*> row1(3, nullptr);
    row1.at(0) =  new EdgeOfForest("Edge of forest");
    row1.at(1) = new Lake("Lake");
    row1.at(2) = new Cliff("Cliff");

    std::vector<Location*> row2(3, nullptr);
    row2.at(0) = new Forest("Forest");
    row2.at(1) = new Beach("Beach");
    row2.at(2) = new Mountain("Mountain");

    std::vector<Location*> row3(3, nullptr);
    row3.at(0) = new DarkForest("Dark forest");
    row3.at(1) = new Start("Start");
    row3.at(2) = new Meadow("Meadow");

    m_locations = {row0, row1, row2, row3};
}


void Map::assignLocationPointers(){
    ///edge of village

///problem in here;assigned locations not working; dont know why; mby i will cut it out of game;
   m_locations.at(0).at(0)->setLocationSouth(m_locations.at(1).at(0));
   m_locations.at(0).at(0)->setLocationWest(m_locations.at(0).at(1));



   ///boss
    m_locations.at(0).at(1)->setLocationWest(m_locations.at(0).at(2));
    m_locations.at(0).at(1)->setLocationEast(m_locations.at(0).at(0));

    ///village
    m_locations.at(0).at(2)->setLocationSouth(m_locations.at(1).at(2));
    m_locations.at(0).at(2)->setLocationEast(m_locations.at(0).at(1));

    ///edge of forest
    m_locations.at(1).at(0)->setLocationSouth(m_locations.at(2).at(0));
    m_locations.at(1).at(0)->setLocationNorth(m_locations.at(0).at(0));
    m_locations.at(1).at(0)->setLocationEast(m_locations.at(1).at(1));  ///lake to be done

    ///cliff
    m_locations.at(1).at(2)->setLocationSouth(m_locations.at(2).at(2));
    m_locations.at(1).at(2)->setLocationNorth(m_locations.at(0).at(2));
    m_locations.at(1).at(2)->setLocationWest(m_locations.at(1).at(1));  ///lake to be done

    ///forest
    m_locations.at(2).at(0)->setLocationSouth(m_locations.at(3).at(0));
    m_locations.at(2).at(0)->setLocationEast(m_locations.at(2).at(1));
    m_locations.at(2).at(0)->setLocationNorth(m_locations.at(1).at(0));

    ///beach
    m_locations.at(2).at(1)->setLocationSouth(m_locations.at(3).at(1));
    m_locations.at(2).at(1)->setLocationNorth(m_locations.at(1).at(1)); ///lake to be done
    m_locations.at(2).at(1)->setLocationEast(m_locations.at(2).at(2));
    m_locations.at(2).at(1)->setLocationWest(m_locations.at(2).at(0));

    ///mountain
    m_locations.at(2).at(2)->setLocationSouth(m_locations.at(3).at(2));
    m_locations.at(2).at(2)->setLocationWest(m_locations.at(2).at(1));
    m_locations.at(2).at(2)->setLocationNorth(m_locations.at(1).at(2));

    ///dark forest
    m_locations.at(3).at(0)->setLocationNorth(m_locations.at(2).at(0));

    ///start
    m_locations.at(3).at(1)->setLocationNorth(m_locations.at(2).at(1));
    m_locations.at(3).at(1)->setLocationEast(m_locations.at(3).at(2));

    ///meadow
    m_locations.at(3).at(2)->setLocationNorth(m_locations.at(2).at(2));
    m_locations.at(3).at(2)->setLocationWest(m_locations.at(3).at(1));
}


Location * Map::getLocation(int row, int column) {return m_locations.at(row).at(column);}

void Map::printTopBorder(int width, char character){
    for (int i = 0; i < width; i++) {
        std::cout << character;
    }
    std::cout << std::endl;
}

void Map::printMap(Location* currentHeroLocation) {
    char const border = '-';
    char const separator = ' ';
    int const width = 20;
    int const mapWidth = 64;

    printTopBorder(mapWidth, border);

    for (auto row:m_locations) {
        std::cout << "| ";
        for(auto column:row){
            if (currentHeroLocation == column){
                std::cout << std::left << std::setw(width) << std::setfill(separator) << "You are HERE!";
            }else{
                std::cout << std::left << std::setw(width) << std::setfill(separator) << column->getLocationName();
            }
        }
        std::cout << " |" << std::endl;
    }
    printTopBorder(mapWidth, border);
}

Map::~Map() {
    for(auto row:m_locations){
        for (auto column:row) {
            delete column;
        }
    }
}

Map *Map::getMap() {
    if (s_map == nullptr){
        s_map = new Map();
    }
    return s_map;
}
