#ifndef GAME_LOCATION_H
#define GAME_LOCATION_H
#include <iostream>
#include "Monsters/Monster.h"
#include "Hero/Key.h"
#include "Hero/Armor.h"
#include "Hero/Weapon.h"
#include "Hero/Potion.h"


class Location {
    std::string m_locationName;
protected:
    bool m_sideQuestActivated;
    Monster* m_monster;

    Armor* m_foundArmor;
    Weapon* m_foundWeapon;
    Potion* m_foundPotion;

    Location* m_north;
    Location* m_east;
    Location* m_south;
    Location* m_west;

public:
    Location(std::string locationName);

    std::string getLocationName();

    void setLocationNorth(Location* pointer);
    void setLocationEast(Location* pointer);
    void setLocationWest(Location* pointer);
    void setLocationSouth(Location* pointer);

    Location* getNorthLocation();
    Location* getEastLocation();
    Location* getSouthLocation();
    Location* getWestLocation();

    Monster* getMonster();
    Armor* getFoundArmor();
    Weapon* getFoundWeapon();
    Potion* getFoundPotion();

    void loseArmor();
    void loseWeapon();
    void losePotion();

    bool checkSideQuest();
    virtual void sideQuest();


    virtual ~Location();
};


#endif //GAME_LOCATION_H
