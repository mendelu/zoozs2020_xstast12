#ifndef GAME_COMBAT_H
#define GAME_COMBAT_H

#include "Hero/Hero.h"

class Combat {
    Hero* m_hero;
    Monster* m_monster;
public:
    Combat(Hero* hero, Monster* monster);
    void fight();
    int getPlayerInput();


};


#endif //GAME_COMBAT_H
