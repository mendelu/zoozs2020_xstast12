#ifndef GAME_GAME_H
#define GAME_GAME_H

#include <iostream>
#include "Hero/Hero.h"
#include "Map.h"
#include "Combat.h"
#include "Interactions/LightAttack.h"
#include "Interactions/Defense.h"
#include "Interactions/HeavyAttack.h"


class Game {
    Hero* m_hero;
    Map* m_map;
public:
    Game();

    void playersOptionsMenu();
    void chooseOption();
    int checkSelectedOption();

    void whereCanTravel();
    void printWhereCanTravel(Location* location, std::string direction, char ch);
    int checkSelectedTravelOption();

    int printAroundItems();
    void lookAround();
    int checkLookAroundInput();
};


#endif //GAME_GAME_H
