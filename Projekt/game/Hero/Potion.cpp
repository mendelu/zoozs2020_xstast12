#include "Potion.h"

Potion::Potion(std::string potionName, int hpoints, int apoints){
    m_potionName = potionName;
    m_healthPoints = hpoints;
    m_attackPoints = apoints;
}

int Potion::getHP() {return m_healthPoints;}
int Potion::getAP() {return m_attackPoints;}

std::string Potion::getPotionName() {return m_potionName;}
