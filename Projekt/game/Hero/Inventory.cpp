#include "Inventory.h"

Inventory::Inventory(){}

void Inventory::addPotion(Potion* potion){
    m_listOfPotions.push_back(potion);
}

void Inventory::addKey(Key *key) {
    m_listOfKeys.push_back(key);
}



Inventory::~Inventory() {
    for (auto key:m_listOfKeys) {
        delete key;
    }

    for (auto potion:m_listOfPotions) {
        delete potion;
    }
}

Potion* Inventory::getLastPotion() {
    if (m_listOfPotions.empty()){
        return nullptr;
    }
    return m_listOfPotions.back();
}

int Inventory::keysListSize() {
    return m_listOfKeys.size();
}

void Inventory::emptyLastPotion() {
    m_listOfPotions.pop_back();
}


