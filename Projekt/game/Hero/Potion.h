#ifndef GAME_POTION_H
#define GAME_POTION_H

#include <iostream>

///I wanted to use inheritance, but having 2 additional classes just for 1 method is kinda useless;

class Potion {
    std::string m_potionName;
    int m_healthPoints;
    int m_attackPoints;
public:
    Potion(std::string potionName, int hpoints, int apoints);
    int getHP();
    int getAP();
    std::string getPotionName();


};


#endif //GAME_POTION_H
