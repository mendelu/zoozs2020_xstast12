#ifndef GAME_HERO_H
#define GAME_HERO_H

#include <vector>
#include <iostream>
#include "Inventory.h"
#include "../Location.h"
#include "Weapon.h"
#include "Armor.h"
#include "../Interactions/Interaction.h"



class Hero {
    std::string m_name;
    int m_healthPoints;
    int m_attack;
    int m_defensePoints;

    Location* m_currentLocation;

    Weapon* m_equipedWeapon;
    Armor* m_equipedArmor;

    Inventory* m_backpack;

    std::vector<Interaction*> m_interactions;

    void setHeroName();
    void setBeginningStats();
public:
    Hero();


    int getAttack();
    int getHealth();
    int getDefensePoints();
    void addDefensePoints();
    void resetDefensePoints();
    void takeDamage(int damage);

    Weapon* getWeapon();
    Armor* getArmor();
    Inventory* getInventory();

    void addHealth(int points);
    void addAttack(int points);


    void travel(Location* location);
    Location* getLocation();

    void pickUpWeapon(Weapon* weapon);
    void pickUpArmor(Armor* armor);

    void pickUpPotion(Potion* potion);
    void drinkPotion();


    void addInteraction(Interaction* interaction);
    void interact(int position, Monster* monster);
    void printInteractions();
    int getInteractionsCount();
    


    ~Hero();
};


#endif //GAME_HERO_H
