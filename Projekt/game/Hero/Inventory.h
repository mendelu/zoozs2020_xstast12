#ifndef GAME_INVENTORY_H
#define GAME_INVENTORY_H

#include <iostream>
#include <vector>
#include "Key.h"
#include "Potion.h"

class Inventory {
    std::vector<Key*> m_listOfKeys;
    std::vector<Potion*> m_listOfPotions;

public:
    Inventory();

    void addPotion(Potion* potion);
    void addKey(Key* key);

    Potion* getLastPotion();
    void emptyLastPotion();
    int keysListSize();

    ~Inventory();
};


#endif //GAME_INVENTORY_H
