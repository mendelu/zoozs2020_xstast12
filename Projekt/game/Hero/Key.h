#ifndef GAME_KEY_H
#define GAME_KEY_H

#include <iostream>

class Key {
    std::string m_description;
public:
    Key(std::string description);
    void printDescription();
};


#endif //GAME_KEY_H
