#ifndef GAME_ARMOR_H
#define GAME_ARMOR_H
#include <iostream>

class Armor {
    int m_healthBonus;
    std::string m_armorName;
public:
    Armor(int bonus, std::string name);

    int geArmorBonus();
    std::string getArmorName();
};


#endif //GAME_ARMOR_H
