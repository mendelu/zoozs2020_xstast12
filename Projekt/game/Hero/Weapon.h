#ifndef GAME_WEAPON_H
#define GAME_WEAPON_H

#include <iostream>


class Weapon {
    int m_attackBonus;
    std::string m_weaponName;
public:
    Weapon(int damage, std::string name);

    int getAttackBonus();
    std::string getWeaponName();
};


#endif //GAME_WEAPON_H
