#include "Hero.h"

void Hero::setHeroName(){
    std::string name;
    std::cout << "Choose a name for your hero:" << std::endl;
    std::cin >> name;
    while (name == ""){
        std::cout << "Please enter valid name for your hero,"
                     " otherwise he will be retarded!" << std::endl;
        std::cin >> name;
    }
    m_name = name;
    std::cout << "Great choice"  << ", I used to be an adventurer like " << name
                    << ", but then I took an arrow in the knee!" <<  std::endl;
}

void Hero::setBeginningStats() {
    char stats;
    std::cout << "Now choose your beginning stats." << std::endl;
    std::cout << "Do you want more HEALTH[h] or ATTACK[a] ?" << std::endl;
    std::cin >> stats;

    switch (stats) {
        case 'h':
            std::cout << "You are going to have more health!" << std::endl;
            m_healthPoints = 90;
            m_attack = 10;
            break;

        case 'a':
            std::cout << "You are going to be strong!" << std::endl;
            m_healthPoints = 80;
            m_attack = 20;
            break;

        default:
            std::cout << "You are going to be retarded!" << std::endl;
            m_healthPoints = 10;
            m_attack = 1;
    }
}

Hero::Hero() {
    setHeroName();
    setBeginningStats();

    /// not sure if this is ok but it should work, otherwise change into creation and then assign to m_equipidWeapon;
    m_equipedWeapon = new Weapon(10, "Rusty knife");
    m_equipedArmor = new Armor(10, "Rusty handcuffs");
    m_backpack = new Inventory();
    m_currentLocation = nullptr;
    m_defensePoints = 0;


}

int Hero::getAttack(){return m_attack;}
int Hero::getHealth(){return m_healthPoints;}

void Hero::addHealth(int points) {m_healthPoints += points;}
void Hero::addAttack(int points) {m_attack = points;}

void Hero::travel(Location *location) {m_currentLocation = location;}
Location* Hero::getLocation(){return m_currentLocation;}

void Hero::pickUpWeapon(Weapon* weapon){
    delete m_equipedWeapon;
    m_equipedWeapon = weapon;
    std::cout << "You picked up your new weapon.\n";
}

void Hero::pickUpArmor(Armor* armor){
    delete m_equipedArmor;
    m_equipedArmor = armor;
    std::cout << "You picked up your new armor.\n";
}

void Hero::pickUpPotion(Potion* potion){
    std::cout << "You picked up potion.\n";
    m_backpack->addPotion(potion);
}

void Hero::drinkPotion() {
    if (m_backpack->getLastPotion() != nullptr){
        Potion* currentPotion = m_backpack->getLastPotion();
        m_healthPoints += currentPotion->getHP();
        m_attack += currentPotion->getAP();
        std::cout << "You drank your last " << currentPotion->getPotionName()
        << ", and gained " << currentPotion->getHP() << " HP and "<< currentPotion->getAP() << " AP.\n";
        m_backpack->emptyLastPotion();
        delete currentPotion;

    } else{
        std::cout << "You dont have any potions!" << std::endl;
    }
}


Hero::~Hero() {
    delete m_equipedWeapon;
    delete m_equipedArmor;
    delete m_backpack;
    /*
    for (int i = 0; i < m_interactions.size(); i++) {
        delete m_interactions.at(i);
    }
     */
}

void Hero::takeDamage(int damage) {
    m_healthPoints -= damage;
    ///std::cout << "Your health decreased by " << damage << std::endl;
}

Weapon *Hero::getWeapon() {
    return m_equipedWeapon;
}

Armor *Hero::getArmor() {return m_equipedArmor;}

Inventory *Hero::getInventory() {
    return m_backpack;
}

int Hero::getDefensePoints() {
    return m_defensePoints;
}

void Hero::addDefensePoints() {
    m_defensePoints += 10;
}

void Hero::resetDefensePoints(){m_defensePoints = 0;}

void Hero::addInteraction(Interaction* interaction){m_interactions.push_back(interaction);}

void Hero::interact(int position, Monster* monster){m_interactions.at(position)->Interact(monster);}

void Hero::printInteractions(){
    std::cout << "Your interactions:\n";
    for(int i = 0; i<m_interactions.size(); i++){
        std::cout << " [" << i << "]    " << m_interactions.at(i)->getDescription() << std::endl;
    }
}

int Hero::getInteractionsCount() {
    return m_interactions.size();
}
