#include "Weapon.h"

Weapon::Weapon(int damage, std::string name){
    m_attackBonus = damage;
    m_weaponName = name;
}

int Weapon::getAttackBonus(){
    return m_attackBonus;
}
std::string Weapon::getWeaponName(){
    return m_weaponName;
}
