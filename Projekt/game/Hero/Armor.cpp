#include "Armor.h"

Armor::Armor(int bonus, std::string name) {
    m_healthBonus = bonus;
    m_armorName = name;
}

int Armor::geArmorBonus(){
    return m_healthBonus;
}
std::string Armor::getArmorName(){
    return m_armorName;
}