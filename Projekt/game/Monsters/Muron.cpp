#include "Muron.h"

Muron::Muron():Monster("Muron", 110, 10, 10){
    m_key = new Key("Second key to Mendelu fortress, dropped by Muron.");
}
void Muron::monsterDescription() {
    std::cout << "You encountered a vicious beast Mikuláš Muroň!" << std::endl;
    std::cout << "Although his strength is not as mighty, he is well known to talk his preys to death." << std::endl;
    std::cout << "Dueling him might prove to be a difficult task, but fight tirelessly and you will take this!" << std::endl;
}





