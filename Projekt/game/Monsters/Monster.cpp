#include "Monster.h"

Monster::Monster(std::string monsterName, int health, int attack, int defense){
    m_monsterName = monsterName;
    m_monsterHealth = health;
    m_monsterAttack = attack;
    m_monsterDefense = defense;
    m_life = true;
}

std::string Monster::getMonsterName() {return m_monsterName;}
int Monster::getMonsterHealth(){return m_monsterHealth;}
int Monster::getMonsterAttack(){return m_monsterAttack;}
int Monster::getMonsterDefense() {return m_monsterDefense;}

void Monster::takeDamage(int damage) {
    m_monsterHealth -= damage;
}

Key *Monster::getKey() {
    return m_key;
}

bool Monster::isAlive() {
    return m_life;
}




void Monster::killMonster() {m_life = false;}
