#include "Zejda.h"

Zejda::Zejda():
    Monster("Zejda", 80, 20, 5){
    m_key = new Key("First key to Mendelu fortress, dropped by Zejda.");
}
void Zejda::monsterDescription(){
    std::cout << "You hear loud thumping in the distance. Before you make any decisions, in front of you stands Martin Zejda!" << std::endl;
    std::cout << "Just one look at him is enough for you to know he is a bench-presser." << std::endl;
    std::cout << "Your advantage is te fact that Zejda likes to skip leg day way too often, thus his health is not as high." << std::endl;
    std::cout << "Hit attacks are crushing, but he will not survive much damage himself." << std::endl;
}

