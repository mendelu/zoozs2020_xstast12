#ifndef GAME_MONSTER_H
#define GAME_MONSTER_H

#include <iostream>
#include "../Hero/Key.h"


class Monster {

protected:
    std::string m_monsterName;
    int m_monsterHealth;
    int m_monsterAttack;
    int m_monsterDefense;
    bool m_life;

    Key* m_key;
public:
    Monster(std::string monsterName, int health, int attack, int defense);
    std::string getMonsterName();
    int getMonsterHealth();
    int getMonsterAttack();
    int getMonsterDefense();
    Key* getKey();
    virtual void monsterDescription() = 0;

    bool isAlive();
    void killMonster();

    void takeDamage(int damage);

};


#endif //GAME_MONSTER_H
