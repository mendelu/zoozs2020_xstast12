#ifndef GAME_MURON_H
#define GAME_MURON_H

#include "Monster.h"

class Muron: public Monster {
public:
    Muron();
    void monsterDescription();
};


#endif //GAME_MURON_H
