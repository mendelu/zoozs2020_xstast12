#include "Combat.h"

Combat::Combat(Hero *hero, Monster *monster) {
    m_hero = hero;
    m_monster = monster;

}

void Combat::fight() {

    std::cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\nYou encountered "
    <<m_monster->getMonsterName() << "\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"<< std::endl;


    m_monster->monsterDescription();

    while (m_monster->isAlive()){
        std::cout << "\nWhat do you want to do?\n";
        m_hero->printInteractions();
        int input = getPlayerInput();
        m_hero->interact(input, m_monster);

        ///***nahradit za interakcie monstra;
        if (m_monster->isAlive()){

            m_hero->takeDamage(m_monster->getMonsterAttack());
            std::cout << m_monster->getMonsterName() << " hit you by " << m_monster->getMonsterAttack() << std::endl;
            std::cout << "Your current health is " << m_hero->getHealth() << std::endl;
        }
        ///***


        if (m_hero->getHealth() <= 0){
            std::cout << "You died!\n";
            exit(0);
        }
    }
    m_monster->killMonster();
}

int Combat::getPlayerInput(){
    int input;
    std::cin >> input;
    while (input >= m_hero->getInteractionsCount()){
        std::cout << "Please input valid number!\n";
        std::cin >> input;
    }
    return input;
}


