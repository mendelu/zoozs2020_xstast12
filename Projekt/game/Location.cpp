#include "Location.h"


Location::Location(std::string locationName){
    m_locationName = locationName;
    m_sideQuestActivated = false;
}


void Location::setLocationNorth(Location *pointer) {m_north = pointer;}
void Location::setLocationEast(Location* pointer){m_east = pointer;}
void Location::setLocationWest(Location* pointer){m_west = pointer;}
void Location::setLocationSouth(Location* pointer){m_south = pointer;}


Location* Location::getNorthLocation(){return m_north;}
Location* Location::getEastLocation(){return m_east;}
Location* Location::getSouthLocation(){return m_south;}
Location* Location::getWestLocation(){return m_west;}



std::string Location::getLocationName(){
    return m_locationName;
}

Location::~Location(){
    delete m_monster;
}


Armor *Location::getFoundArmor() {return m_foundArmor;}

Weapon* Location::getFoundWeapon() {return m_foundWeapon;}

Potion *Location::getFoundPotion() {return m_foundPotion;}

Monster* Location::getMonster() {return m_monster;}

void Location::sideQuest() {
    std::cout << "------\nNo sidequest created, blame terrible game designers.\n------\n";
    m_sideQuestActivated = true;
}

bool Location::checkSideQuest(){
    return m_sideQuestActivated;
}

void Location::loseArmor(){m_foundArmor = nullptr;}
void Location::loseWeapon(){m_foundWeapon = nullptr;}
void Location::losePotion(){m_foundPotion = nullptr;}
