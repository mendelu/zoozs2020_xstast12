#include "Village.h"

Village::Village(std::string locationName):
        Location(locationName){
    m_foundPotion = new Potion("Potion of destruction", 100, 200);
    }

void Village::sideQuest() {
    std::cout << "It's almost midnight, but you see light.\nIts local tavern\n.\n..\n...\nYou wake up in the morning\n"
                 "Last night you celebrate!\n";

    std::cout << "Innkeeper thanks you for killing monster\n"
                 "placing a special potion in front of you shouting: DONT FORGET TO TAKE THIS!!!" << std::endl;
    m_sideQuestActivated = true;
}

Village::~Village(){
    delete m_foundPotion;
}