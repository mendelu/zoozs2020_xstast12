#include "Beach.h"

Beach::Beach(std::string locationName):
        Location(locationName){

}

void Beach::sideQuest() {
    std::cout << "You found message in a bottle...\n";
    std::cout << "you opened it and read:  \n";
    std::cout << "I was kidnapped into dark forest by a monster carrying very shiny sword ... \n";
    std::cout << "If you dare, challenge it and SAVE ME!" << std::endl;
    m_sideQuestActivated = true;
}
