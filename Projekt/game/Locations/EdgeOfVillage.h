#ifndef GAME_EDGEOFVILLAGE_H
#define GAME_EDGEOFVILLAGE_H

#include "../Location.h"

class EdgeOfVillage:public Location{
public:
    EdgeOfVillage(std::string locationName);
    void sideQuest();
};


#endif //GAME_EDGEOFVILLAGE_H
