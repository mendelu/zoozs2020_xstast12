#ifndef GAME_BEACH_H
#define GAME_BEACH_H

#include "../Location.h"

class Beach:public Location{
public:
    Beach(std::string locationName);
    void sideQuest();
};


#endif //GAME_BEACH_H
