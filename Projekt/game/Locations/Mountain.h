#ifndef GAME_MOUNTAIN_H
#define GAME_MOUNTAIN_H


#include "../Location.h"

class Mountain:public Location{
public:
    Mountain(std::string locationName);
    void sideQuest();
    ~Mountain();
};


#endif //GAME_MOUNTAIN_H
