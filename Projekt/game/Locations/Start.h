#ifndef GAME_START_H
#define GAME_START_H

#include "../Location.h"

class Start:public Location{
public:
    Start(std::string locationName);
    void sideQuest();
};


#endif //GAME_START_H
