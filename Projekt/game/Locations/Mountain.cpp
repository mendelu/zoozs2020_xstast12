#include "Mountain.h"

Mountain::Mountain(std::string locationName):
        Location(locationName){
    m_foundArmor = new Armor(30, "Something better than yours");
}

void Mountain::sideQuest() {
    std::cout << "Nothing around except for a bounty poster.\nYou look closer and see contract on local monster Muron!\n"
                 "Theres a huge reward on its head.\nCan you challenge it?..." << std::endl;
    std::cout << "Aaaand there is some kind of armor lying down there." << std::endl;
    m_sideQuestActivated = true;
}

Mountain::~Mountain(){delete m_foundArmor;}