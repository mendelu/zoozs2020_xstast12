#include "Start.h"

Start::Start(std::string locationName):
Location(locationName){}

void Start::sideQuest() {
    std::cout << "\nWelcome to our little game! You have some options which you can choose!\n";
    std::cout << "To choose one of the options, simply enter correct letter or number!\n";
    std::cout << "If you find a monster, you will be attacked immediately, so watch out!\n";
    std::cout << "In order to win this game you need to defeat 2 sub-bosses, granting you 2 keys.\n";
    std::cout << "Those keys let you inside Boss location, where obviously you have to defeat final boss." << std::endl;
    m_sideQuestActivated = true;
}
