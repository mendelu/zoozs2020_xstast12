#ifndef GAME_DARKFOREST_H
#define GAME_DARKFOREST_H

#include "../Location.h"
#include "../Monsters/Zejda.h"


class DarkForest: public Location {
public:
    DarkForest(std::string locationName);
    ~DarkForest();
};


#endif //GAME_DARKFOREST_H
