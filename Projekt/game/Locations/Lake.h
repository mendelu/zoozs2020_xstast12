#ifndef GAME_LAKE_H
#define GAME_LAKE_H

#include "../Location.h"

class Lake:public Location{

public:
    Lake(std::string locationName);
    void sideQuest();
};


#endif //GAME_LAKE_H
