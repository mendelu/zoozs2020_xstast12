#ifndef GAME_BOSSLOCATION_H
#define GAME_BOSSLOCATION_H

#include "../Location.h"
#include "../Monsters/Prochazka.h"

class BossLocation:public Location{
public:
    BossLocation(std::string locationName);
    ~BossLocation();
};


#endif //GAME_BOSSLOCATION_H
