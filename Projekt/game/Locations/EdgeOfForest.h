#ifndef GAME_EDGEOFFOREST_H
#define GAME_EDGEOFFOREST_H

#include "../Location.h"

class EdgeOfForest:public Location{
public:
    EdgeOfForest(std::string locationName);
    void sideQuest();
    ~EdgeOfForest();
};


#endif //GAME_EDGEOFFOREST_H
