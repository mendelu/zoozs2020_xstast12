#include "BossLocation.h"

BossLocation::BossLocation(std::string locationName):
        Location(locationName){
    m_monster = new Prochazka();
}

BossLocation::~BossLocation(){
    delete m_monster;
}