#ifndef GAME_MEADOW_H
#define GAME_MEADOW_H


#include "../Location.h"

class Meadow:public Location{
public:
    Meadow(std::string locationName);
};


#endif //GAME_MEADOW_H
