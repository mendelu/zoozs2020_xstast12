#include "DarkForest.h"

DarkForest::DarkForest(std::string locationName):
        Location(locationName){
    m_monster = new Zejda();
    m_foundWeapon = new Weapon(40, "Sword of OOP");
}


DarkForest::~DarkForest(){
    delete m_monster;
    delete m_foundWeapon;
}