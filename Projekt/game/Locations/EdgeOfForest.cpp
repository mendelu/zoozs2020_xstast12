#include "EdgeOfForest.h"

EdgeOfForest::EdgeOfForest(std::string locationName):
        Location(locationName){
    m_foundPotion = new Potion("Potion of strength", 0, 50);
}

void EdgeOfForest::sideQuest() {
    std::cout << "You can see a lot of merchants riding their horses out of there!\n";
    std::cout << "When suddenly, you see that one of them has dropped a potion.\n" << std::endl;
    std::cout << "...\n..\n.\n And also if you travel to the Edge of the Village, something unexpected will happend!\n";
    std::cout << "So do NOT try it!" << std::endl;
    m_sideQuestActivated = true;
}

EdgeOfForest::~EdgeOfForest(){
    delete m_foundPotion;
}