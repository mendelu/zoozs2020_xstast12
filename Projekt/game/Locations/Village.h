#ifndef GAME_VILLAGE_H
#define GAME_VILLAGE_H

#include "../Location.h"

class Village:public Location{
public:
    Village(std::string locationName);
    void sideQuest();
    ~Village();
};


#endif //GAME_VILLAGE_H
