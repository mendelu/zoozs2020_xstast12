#ifndef GAME_CLIFF_H
#define GAME_CLIFF_H

#include "../Location.h"
#include "../Monsters/Muron.h"

class Cliff:public Location{
public:
    Cliff(std::string locationName);
    ~Cliff();
};


#endif //GAME_CLIFF_H
