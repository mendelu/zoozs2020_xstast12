#include "Game.h"

Game::Game() {
    m_map = Map::getMap();
    m_hero = new Hero();
    m_hero->addInteraction(new LightAttack("Light Attack", m_hero));
    m_hero->addInteraction(new HeavyAttack("Heavy Attack", m_hero));
    m_hero->addInteraction(new Defense("Defense (++dmg)", m_hero));
    m_hero->travel(m_map->getLocation(3, 1));

    ///z neznamecho dovodu toto hodi sigseg ale nechapem preco a nerozumiem ze je zrazu v pohode vsetko s hrou;
/*
    std::cout << "North:" << m_map->getLocation(1,1)->getNorthLocation()->getLocationName() << std::endl;
    std::cout << "South: "<< m_map->getLocation(1,1)->getSouthLocation()->getLocationName()<< std::endl;
    std::cout << "West: " << m_map->getLocation(1,1)->getWestLocation()->getLocationName()<< std::endl;
    std::cout << "East: " << m_map->getLocation(1,1)->getEastLocation()->getLocationName()<< std::endl;

*/

    while (m_map->getLocation(0, 1)->getMonster()->isAlive()){
        if (!m_hero->getLocation()->checkSideQuest()){
            m_hero->getLocation()->sideQuest();
        }

        if (m_hero->getLocation()->getMonster() != nullptr && m_hero->getLocation()->getMonster()->isAlive()){
            Combat* combat = new Combat(m_hero, m_hero->getLocation()->getMonster());
            combat->fight();
            delete combat;
        }

        if (m_map->getLocation(0, 1)->getMonster()->isAlive()){
            playersOptionsMenu();
        }
    }

    std::cout << "\nCongratulations, you have won our game!\n";
    std::cout << "Written by Marek Moric and Nikolas Stastny in 2020 as ZOO semestral project.\n";
}

void Game::chooseOption() {
    std::cout << "1. Travel [t]\n";
    std::cout << "2. Look around [l]\n";
    std::cout << "3. Drink your last potion [p]\n";
    std::cout << "4. Open pocket map [m]\n";
    std::cout << "5. Quit game [q]\n" << std::endl;
}

int Game::checkSelectedOption(){
    char input;
    std::cin >> input;

    switch (input) {
        case 't':
            whereCanTravel();
            break;
        case 'l':

            lookAround();
            break;
        case 'p':
            m_hero->drinkPotion();
            break;
        case 'm':
            m_map->printMap(m_hero->getLocation());
            break;
        case 'q':
            std::cout << "You quit the game!\n";
            exit(0);
        default:
            std::cout << "Please enter valid letter!\n";
            return 1;
    }
    return 0;
}

void Game::playersOptionsMenu() {
    std::cout << "\nWhat do you want to do?\n";
    chooseOption();

    while (checkSelectedOption() == 1){}
}

void Game::printWhereCanTravel(Location* location, std::string direction, char ch){
    if(location != nullptr){
        std::cout << location->getLocationName() << " lies to the "
        << direction << " from you! " << '[' << ch << ']' << std::endl;
    }
}



int Game::checkSelectedTravelOption(){
    char input;
    std::cin >> input;

    switch (input) {
        case 'n':
            if (m_hero->getLocation()->getNorthLocation() == nullptr){
                std::cout << "Please enter valid letter!\n";
                return 1;
            }

            std::cout << "You traveled to: " << m_hero->getLocation()->getNorthLocation()->getLocationName() << std::endl;
            m_hero->travel(m_hero->getLocation()->getNorthLocation());
            break;
        case 'e':
                if (m_hero->getLocation()->getEastLocation() == nullptr){
                    std::cout << "Please enter valid letter!\n";
                    return 1;
                }

            if (m_hero->getLocation()->getEastLocation()->getLocationName() == "Boss" &&
                m_hero->getInventory()->keysListSize() < 2) {
                std::cout << "You cannot travel there right now. You dont have enough keys!\n";
            } else{
                std::cout << "You traveled to: " << m_hero->getLocation()->getEastLocation()->getLocationName() << std::endl;
                m_hero->travel(m_hero->getLocation()->getEastLocation());
            }

            break;
        case 's':
            if (m_hero->getLocation()->getSouthLocation() == nullptr){
                std::cout << "Please enter valid letter!\n";
                return 1;
            }
            std::cout << "You traveled to: " << m_hero->getLocation()->getSouthLocation()->getLocationName() << std::endl;
            m_hero->travel(m_hero->getLocation()->getSouthLocation());
            break;
        case 'w':
            if (m_hero->getLocation()->getWestLocation() == nullptr){
                std::cout << "Please enter valid letter!\n";
                return 1;
            }
            if (m_hero->getLocation()->getWestLocation()->getLocationName() == "Boss" &&
                m_hero->getInventory()->keysListSize() < 2) {
                std::cout << "You cannot travel there right now. You dont have enough keys!\n";
            } else{
                std::cout << "You traveled to: " << m_hero->getLocation()->getWestLocation()->getLocationName() << std::endl;
                m_hero->travel(m_hero->getLocation()->getWestLocation());
            }
            break;

        case 'r':
            break;
        default:
            std::cout << "Please enter valid letter!\n";
            return 1;
    }
    return 0;
}

void Game::whereCanTravel() {
    std::cout << "\nWhere do you want to travel?" << std::endl;

    printWhereCanTravel(m_hero->getLocation()->getNorthLocation(), "North", 'n');
    printWhereCanTravel(m_hero->getLocation()->getEastLocation(), "East", 'e');
    printWhereCanTravel(m_hero->getLocation()->getSouthLocation(), "South", 's');
    printWhereCanTravel(m_hero->getLocation()->getWestLocation(), "West", 'w');
    std::cout << "Return [r]\n";

    while (checkSelectedTravelOption()){}
}

int Game::printAroundItems(){
    if (    m_hero->getLocation()->getFoundWeapon() == nullptr &&
            m_hero->getLocation()->getFoundArmor() == nullptr &&
            m_hero->getLocation()->getFoundPotion() == nullptr
            ){
        std::cout << "Nothing there!\n";
        return 0;

    } else{
        std::cout << "Found item:\n";
        if (m_hero->getLocation()->getFoundArmor() != nullptr){
            std::cout << "[a] Armor: " << m_hero->getLocation()->getFoundArmor()->getArmorName() << ", bonus: " <<
            m_hero->getLocation()->getFoundArmor()->geArmorBonus() << std::endl;
        }
        if (m_hero->getLocation()->getFoundWeapon() != nullptr) {
            std::cout << "[w] Weapon: " << m_hero->getLocation()->getFoundWeapon()->getWeaponName() << ", bonus: " <<
                      m_hero->getLocation()->getFoundWeapon()->getAttackBonus() << std::endl;
        }
        if (m_hero->getLocation()->getFoundPotion() != nullptr){
            std::cout << "[p] Potion: " << m_hero->getLocation()->getFoundPotion()->getPotionName() << ", bonus HP: " <<
                      m_hero->getLocation()->getFoundPotion()->getHP() << ", AP: " <<
                      m_hero->getLocation()->getFoundPotion()->getAP() << std::endl;
        }
        std::cout << "[r] Return to menu" << std::endl;
    }
    return 1;
}




void Game::lookAround(){
    if (printAroundItems() == 1){
        while (checkLookAroundInput()){}
    }

}

int Game::checkLookAroundInput() {
    char input;
    std::cin >> input;

    switch (input) {
        case 'a':
            if (m_hero->getLocation()->getFoundArmor() != nullptr){
                m_hero->pickUpArmor(m_hero->getLocation()->getFoundArmor());
                m_hero->getLocation()->loseArmor();
            }
            break;
        case 'w':
            if (m_hero->getLocation()->getFoundWeapon() != nullptr){
                m_hero->pickUpWeapon(m_hero->getLocation()->getFoundWeapon());
                m_hero->getLocation()->loseWeapon();
            }
            break;
        case 'p':
            if (m_hero->getLocation()->getFoundPotion() != nullptr){
                m_hero->pickUpPotion(m_hero->getLocation()->getFoundPotion());
                m_hero->getLocation()->losePotion();
            }
            break;
        case 'r':
            break;
        default:
            std::cout << "Please enter valid letter!\n";
            return 1;
    }
    return 0;
}


