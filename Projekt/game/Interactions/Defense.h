//
// Created by Nick on 9. 12. 2020.
//

#ifndef GAME_DEFENSE_H
#define GAME_DEFENSE_H
#include "Interaction.h"
#include "../Hero/Hero.h"



class Defense: public Interaction{
    Hero* m_hero;
public:
    Defense(std::string description, Hero* hero);
    void Interact(Monster* monster);
};


#endif //GAME_DEFENSE_H
