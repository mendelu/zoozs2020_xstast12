#include "Interaction.h"

Interaction::Interaction(std::string description){
    m_description = description;
    m_monster = nullptr;
}

std::string Interaction::getDescription() {return m_description;}

void Interaction::Interact(Monster* monster) {}
