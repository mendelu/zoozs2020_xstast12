#include "HeavyAttack.h"

HeavyAttack::HeavyAttack(std::string description,Hero* hero) : Interaction(description) {
    m_heavyDamage = 15;
    m_hero = hero;}

void HeavyAttack::Interact(Monster* monster) {
    int actualDamage = m_heavyDamage + m_hero->getAttack()
                       + m_hero->getWeapon()->getAttackBonus() + m_hero->getDefensePoints();

    if (actualDamage >= monster->getMonsterHealth()){
        std::cout << "You hit " << monster->getMonsterName() << " and dealt " << actualDamage << " damage!" << std::endl;
        std::cout << "You just killed " << monster->getMonsterName() << "!!!" << std::endl;
        monster->killMonster();

        if (monster->getKey() != nullptr){
            std::cout << "You received a key, you can find it in your backpack!" << std::endl;
            m_hero->getInventory()->addKey(monster->getKey());
        }

        delete monster;
        m_hero->resetDefensePoints();

    } else{
        monster->takeDamage(actualDamage);
        std::cout << "You hit " << monster->getMonsterName() << " and dealt " << actualDamage << " damage!" << std::endl;
    }
}