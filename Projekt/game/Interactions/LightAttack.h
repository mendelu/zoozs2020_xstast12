#ifndef GAME_LIGHTATTACK_H
#define GAME_LIGHTATTACK_H

#include "Interaction.h"
#include "../Hero/Hero.h"


class LightAttack: public Interaction {
    int m_lightDamage;
    Hero* m_hero;
public:
    LightAttack(std::string description, Hero* hero);
    void Interact(Monster* monster);

};


#endif //GAME_LIGHTATTACK_H
