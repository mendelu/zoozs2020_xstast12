//
// Created by Nick on 9. 12. 2020.
//

#include "Defense.h"

Defense::Defense(std::string description, Hero* hero):Interaction(description) {
    m_hero = hero;
}

void Defense::Interact(Monster* monster) {
    m_hero->addDefensePoints();
}
