#ifndef GAME_INTERACTION_H
#define GAME_INTERACTION_H

#include <iostream>
#include "../Monsters/Monster.h"

class Interaction {
protected:
    std::string m_description;
    Monster* m_monster;
public:
    Interaction(std::string description);
    virtual void Interact(Monster* monster);
    std::string getDescription();

};


#endif //GAME_INTERACTION_H
