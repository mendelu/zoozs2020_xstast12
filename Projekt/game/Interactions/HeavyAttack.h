#ifndef GAME_HEAVYATTACK_H
#define GAME_HEAVYATTACK_H

#include "Interaction.h"
#include "../Hero/Hero.h"


class HeavyAttack: public Interaction {
    int m_heavyDamage;
    Hero* m_hero;
public:
    HeavyAttack(std::string description, Hero* hero);
    void Interact(Monster* monster);

};



#endif //GAME_HEAVYATTACK_H
