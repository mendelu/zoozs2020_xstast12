#ifndef GAME_MAP_H
#define GAME_MAP_H

#include <iostream>
#include <iomanip>
#include <vector>
#include "Location.h"
#include "Locations/DarkForest.h"
#include "Locations/Start.h"
#include "Locations/Meadow.h"
#include "Locations/Forest.h"
#include "Locations/Beach.h"
#include "Locations/Mountain.h"
#include "Locations/EdgeOfForest.h"
#include "Locations/Lake.h"
#include "Locations/Cliff.h"
#include "Locations/EdgeOfVillage.h"
#include "Locations/BossLocation.h"
#include "Locations/Village.h"


class Map {
    static Map* s_map;
    std::vector<std::vector<Location*>> m_locations;

    Map();
    void createLocations();
    void assignLocationPointers();
public:
    static Map* getMap();

    Location* getLocation(int row, int column);
    void printTopBorder(int width, char character);
    void printMap(Location* currentHeroLocation);

    ~Map();
};


#endif //GAME_MAP_H
