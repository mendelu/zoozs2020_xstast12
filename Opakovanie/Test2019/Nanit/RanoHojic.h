//
// Created by Nick on 3. 1. 2021.
//

#ifndef TEST2019_RANOHOJIC_H
#define TEST2019_RANOHOJIC_H
#include "NanitBuilder.h"
#include "Morfin.h"

class RanoHojic: public NanitBuilder {
    int m_pocetLatok;
    int m_sila;
public:
    RanoHojic(int pocetLatok, int sila);
    void addVystroj();
};


#endif //TEST2019_RANOHOJIC_H
