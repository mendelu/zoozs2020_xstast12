//
// Created by Nick on 3. 1. 2021.
//

#include "Kapsicin.h"

Kapsicin::Kapsicin(int pocetLatok, float ucinnost, int sila):Ampule(pocetLatok) {
        m_ucinnost = ucinnost;
        m_sila = sila;
}


int Kapsicin::getUtocnaSila(){
    return (m_pocetLatek*m_sila)/m_ucinnost;
}
