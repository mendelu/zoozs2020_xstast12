//
// Created by Nick on 3. 1. 2021.
//

#ifndef TEST2019_NANITBUILDER_H
#define TEST2019_NANITBUILDER_H
#include "NanitChripkovy.h"

class NanitBuilder {
protected:
    NanitChripkovy* m_nanitChripkovy;
public:
    NanitBuilder();
    void createNewNanit(std::string oznacenie);

    virtual void addVystroj()=0;

    NanitChripkovy* getNanit();
};


#endif //TEST2019_NANITBUILDER_H
