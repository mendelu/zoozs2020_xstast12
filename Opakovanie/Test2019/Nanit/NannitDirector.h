//
// Created by Nick on 3. 1. 2021.
//

#ifndef TEST2019_NANNITDIRECTOR_H
#define TEST2019_NANNITDIRECTOR_H
#include "NanitBuilder.h"


class NannitDirector {
    NanitBuilder* m_builder;
public:
    NannitDirector(NanitBuilder* builder);
    void setNanitBuilder(NanitBuilder* builder);
    NanitChripkovy* constructNanitChripkovy(std::string oznacenie);
};


#endif //TEST2019_NANNITDIRECTOR_H
