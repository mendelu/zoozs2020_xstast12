//
// Created by Nick on 3. 1. 2021.
//

#include "Vymitac.h"

void Vymitac::addVystroj() {
    m_nanitChripkovy->addVyzbro(new Kapsicin(m_pocetLatok, m_ucinnost, m_sila));
}

Vymitac::Vymitac(int pocetLatok, int sila, float ucinnost) {
    m_sila = sila;
    m_pocetLatok = pocetLatok;
    m_ucinnost = ucinnost;
}
