//
// Created by Nick on 3. 1. 2021.
//

#include "NanitChripkovy.h"

NanitChripkovy::NanitChripkovy(std::string oznacenie) {
    m_oznacenie = oznacenie;
    m_utocnaSila = 40;
    m_vyzbroj = nullptr;
}

void NanitChripkovy::printInfo() {
    std::cout << "NanitChripkovy: " << m_oznacenie << ", utocna sila: "
    << m_utocnaSila+m_vyzbroj->getUtocnaSila() << std::endl;
}

void NanitChripkovy::addVyzbro(Ampule *ampule) {
    m_vyzbroj = ampule;
}
