//
// Created by Nick on 3. 1. 2021.
//

#include "NanitBuilder.h"

NanitBuilder::NanitBuilder() {
 m_nanitChripkovy = nullptr;
}

void NanitBuilder::createNewNanit(std::string oznacenie) {
    m_nanitChripkovy = new NanitChripkovy(oznacenie);
}

NanitChripkovy *NanitBuilder::getNanit() {
    return m_nanitChripkovy;
}
