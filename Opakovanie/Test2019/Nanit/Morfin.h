//
// Created by Nick on 3. 1. 2021.
//

#ifndef TEST2019_MORFIN_H
#define TEST2019_MORFIN_H
#include "Ampule.h"

class Morfin: public Ampule {
    int m_sila;
public:
    Morfin(int pocetLatok, int sila);
    int getUtocnaSila();
};


#endif //TEST2019_MORFIN_H
