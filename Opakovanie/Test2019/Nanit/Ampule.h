//
// Created by Nick on 3. 1. 2021.
//

#ifndef TEST2019_AMPULE_H
#define TEST2019_AMPULE_H
#include <iostream>

class Ampule {
protected:
    int m_pocetLatek;
public:
    Ampule(int pocetlatek);
    virtual int getUtocnaSila()=0;
};


#endif //TEST2019_AMPULE_H
