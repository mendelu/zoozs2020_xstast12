//
// Created by Nick on 3. 1. 2021.
//

#ifndef TEST2019_VYMITAC_H
#define TEST2019_VYMITAC_H
#include "NanitBuilder.h"
#include "Kapsicin.h"

class Vymitac: public NanitBuilder {
    int m_pocetLatok;
    int m_sila;
    float m_ucinnost;
public:
    Vymitac(int pocetLatok, int sila, float ucinnost);
    void addVystroj();
};


#endif //TEST2019_VYMITAC_H
