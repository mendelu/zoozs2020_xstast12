//
// Created by Nick on 3. 1. 2021.
//

#ifndef TEST2019_NANITCHRIPKOVY_H
#define TEST2019_NANITCHRIPKOVY_H
#include <iostream>
#include "Ampule.h"

class NanitChripkovy {
    std::string m_oznacenie;
    int m_utocnaSila;
    Ampule* m_vyzbroj;
public:
    NanitChripkovy(std::string oznacenie);
    void addVyzbro(Ampule* ampule);
    void printInfo();

};


#endif //TEST2019_NANITCHRIPKOVY_H
