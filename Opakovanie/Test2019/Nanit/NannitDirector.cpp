//
// Created by Nick on 3. 1. 2021.
//

#include "NannitDirector.h"

NannitDirector::NannitDirector(NanitBuilder *builder) {
    m_builder = builder;
}

void NannitDirector::setNanitBuilder(NanitBuilder *builder) {m_builder = builder;}

NanitChripkovy * NannitDirector::constructNanitChripkovy(std::string oznacenie) {
    m_builder->createNewNanit(oznacenie);
    m_builder->addVystroj();
    return m_builder->getNanit();
}