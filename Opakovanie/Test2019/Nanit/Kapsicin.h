//
// Created by Nick on 3. 1. 2021.
//

#ifndef TEST2019_KAPSICIN_H
#define TEST2019_KAPSICIN_H
#include "Ampule.h"

class Kapsicin: public Ampule {
    float m_ucinnost;
    int m_sila;
public:
    Kapsicin(int pocetLatok, float ucinnost, int sila);
    int getUtocnaSila();
};


#endif //TEST2019_KAPSICIN_H
