//
// Created by Nick on 3. 1. 2021.
//

#ifndef TEST2019_UZITKOVEAUTO_H
#define TEST2019_UZITKOVEAUTO_H
#include "Auto.h"

class UzitkoveAuto: public Auto {
    int m_hmotnost;
    int m_objemNadrze;
    int m_dojazd;
public:
    UzitkoveAuto(std::string nazov, int objem, int spotreba, int hmotnost, int nadrz);
    void printInfo();
};


#endif //TEST2019_UZITKOVEAUTO_H
