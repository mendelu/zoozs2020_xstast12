//
// Created by Nick on 3. 1. 2021.
//

#include "UzitkoveAuto.h"


UzitkoveAuto::UzitkoveAuto(std::string nazov, int objem, int spotreba, int hmotnost, int nadrz):Auto(nazov, objem, spotreba) {
    m_hmotnost = hmotnost;
    m_objemNadrze = nadrz;
    m_dojazd = m_objemNadrze/spotreba;
}

void UzitkoveAuto::printInfo() {
    std::cout << "Uzitkove auto hmotnost " << m_hmotnost << ", objem nadrze "
              << m_objemNadrze << ", dojazd: " << m_dojazd << std::endl;
}
