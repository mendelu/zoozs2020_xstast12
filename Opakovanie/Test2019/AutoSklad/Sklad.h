//
// Created by Nick on 3. 1. 2021.
//

#ifndef TEST2019_SKLAD_H
#define TEST2019_SKLAD_H
#include "Auto.h"
#include <vector>

class Sklad {
    int m_pocetMiest;
    std::vector<Auto*> m_zoznamAut;
public:
    Sklad(int pocetMiest);
    void pridajAuto(Auto* automobil);
    void printInfo();
    ~Sklad();
};


#endif //TEST2019_SKLAD_H
