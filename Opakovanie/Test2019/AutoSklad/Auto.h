//
// Created by Nick on 3. 1. 2021.
//

#ifndef TEST2019_AUTO_H
#define TEST2019_AUTO_H
#include "iostream"

class Auto {
    std::string m_nazov;
    int m_objemMotoru;
    int m_spotreba;
public:
    Auto(std::string nazov, int objem, int spotreba);
    std::string getNazov();
    int getObjem();
    int getSpotreba();
    virtual void printInfo(){};
};


#endif //TEST2019_AUTO_H
