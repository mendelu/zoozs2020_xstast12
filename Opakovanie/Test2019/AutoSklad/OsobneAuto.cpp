//
// Created by Nick on 3. 1. 2021.
//

#include "OsobneAuto.h"

OsobneAuto::OsobneAuto(std::string nazov, int objem, int spotreba, int sedadla, int uloznyPristor):Auto(nazov, objem, spotreba) {
    m_sedadla = sedadla;
    m_uloznyPriestor = uloznyPristor;
    m_euronorma = (getObjem()*m_sedadla)/getSpotreba();
}

void OsobneAuto::printInfo() {
    std::cout << "Osobne auto sedadla " << m_sedadla << ", ulozny priestor "
    << m_uloznyPriestor << ", euronorma: " << m_euronorma << std::endl;
}
