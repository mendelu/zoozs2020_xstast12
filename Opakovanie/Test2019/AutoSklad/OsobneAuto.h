//
// Created by Nick on 3. 1. 2021.
//

#ifndef TEST2019_OSOBNEAUTO_H
#define TEST2019_OSOBNEAUTO_H
#include "Auto.h"

class OsobneAuto: public Auto {
    int m_sedadla;
    int m_uloznyPriestor;
    int m_euronorma;
public:
    OsobneAuto(std::string nazov, int objem, int spotreba, int sedadla, int uloznyPristor);
    void printInfo();
};


#endif //TEST2019_OSOBNEAUTO_H
