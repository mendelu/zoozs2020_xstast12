//
// Created by Nick on 3. 1. 2021.
//

#include "Sklad.h"

Sklad::Sklad(int pocetMiest) {
    m_pocetMiest = pocetMiest;
}

void Sklad::pridajAuto(Auto *automobil) {
    if (m_zoznamAut.size() < m_pocetMiest){
        m_zoznamAut.push_back(automobil);
    } else{
        std::cout << "Auto neni kam zaparkovat\n";
    }
}

void Sklad::printInfo() {
    std::cout << "Vypis aut:\n";
    for (auto automobil:m_zoznamAut) {
        std::cout << "---------\n";
        automobil->printInfo();
        std::cout << "---------\n";
    }
    std::cout << "Koniec vypisu aut." << std::endl;
}

Sklad::~Sklad() {
    for (auto automobil:m_zoznamAut) {
        delete automobil;
    }
}
