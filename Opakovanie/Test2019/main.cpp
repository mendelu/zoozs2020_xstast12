#include <iostream>
#include "Nanit/NannitDirector.h"
#include "Nanit/Vymitac.h"
#include "Nanit/RanoHojic.h"

#include "AutoSklad/Sklad.h"
#include "AutoSklad/OsobneAuto.h"
#include "AutoSklad/UzitkoveAuto.h"


int main() {

    ///  --- Prva cast
    /*

    NannitDirector* director = new NannitDirector(new RanoHojic(5, 10));
    NanitChripkovy* test1 = director->constructNanitChripkovy("test1");
    test1->printInfo();

    director->setNanitBuilder(new Vymitac(14, 10, 7.5));
    NanitChripkovy* test2 = director->constructNanitChripkovy("Test2");
    test2->printInfo();
    */


    ///   --- Druha cast

    Sklad* sklad = new Sklad(3);
    Auto* os1 = new OsobneAuto("auto1", 50, 20, 5, 10);
    Auto* os2 = new OsobneAuto("auto2", 30, 80, 2, 5);

    Auto* uz1 = new UzitkoveAuto("uzitkove1", 50, 50,50,50);
    Auto* uz2 = new UzitkoveAuto("uzitkove2", 10, 10,10,10);

    sklad->pridajAuto(os1);
    sklad->pridajAuto(os2);
    sklad->pridajAuto(uz1);
    sklad->pridajAuto(uz2);

    sklad->printInfo();
    return 0;
}
