#include <iostream>
using namespace std;

    // cviko 1

class Planet{
    long long m_weight;
    unsigned long m_diameter;
    float m_percentageOfEarth;

public:
    long long getWeight(){
        return m_weight;
    }

    unsigned long getDiameter(){
        return m_diameter;
    }

    float getPercenategeOfEarth(){
        return m_percentageOfEarth;
    }

    float getPercenategeOfWater(){
        return 100 - m_percentageOfEarth;
    }

    void setEarthPercentage(float percentage){
        m_percentageOfEarth = percentage;
    }

    void print(){
        cout << "Planeta ma vahu: " << m_weight << ", priemer: " << m_diameter << ", a percento zeme: "
        << m_percentageOfEarth << ", vody: " << getPercenategeOfWater() << endl;
    }

    Planet(long long weight, unsigned long diameter, float percentage){
        m_percentageOfEarth = percentage;
        m_weight = weight;
        m_diameter = diameter;
    }
};

    //cviko 2

class Student{
    string m_meno;
    long long m_rodneCislo;
    string m_bydlisko;

public:
    Student(string meno, long long rc, string bydlisko){
        m_meno = meno;
        m_rodneCislo = rc;
        m_bydlisko = bydlisko;
    }

    Student(string meno, long long rc):Student(meno, rc, "Nezname"){};

    void setBydlisko(string bydlisko){
        if(bydlisko != ""){
            m_bydlisko = bydlisko;
        }else{
            m_bydlisko = "Neznamo";
        }
    }

    void print(){
        cout << "Student: " << m_meno << ", rodne cislo: " << m_rodneCislo << ", bydlisko: " << m_bydlisko  << endl;
    }
};


class Automobil{
    string m_SPZ;
    int m_najazdeneKm;
    string m_predchMajitel;

public:
    Automobil(string spz, int pocetKm, string majitel){
        m_SPZ = spz;
        m_predchMajitel = majitel;
        m_najazdeneKm = pocetKm;
    }

    Automobil(string spz, int pocetKm):Automobil(spz, pocetKm, "Nezname"){};
    Automobil(string spz, string maitel):Automobil(spz, 0, maitel){};
    Automobil(int pocetKm, string majitel):Automobil("Nezname", pocetKm, majitel){};
    Automobil(int pocetKm):Automobil("nezname", pocetKm, "Nezname"){};
};

//    cviko 3

class Auto{
    int m_najetoKm;
    float m_cenaNaDen;
    float m_sumaVydelku;
    int m_opotrebenie;

public:
    Auto(int najeto, float cena, float suma){
        m_najetoKm = najeto;
        m_cenaNaDen = cena;
        m_sumaVydelku = suma;
        m_opotrebenie = 0;
    }

    float rent(int pocetDni){
        return m_cenaNaDen * pocetDni;
    }

    void zapujceni(int najeto, int pocetDni){
        m_najetoKm += najeto;
        m_opotrebenie += najeto;
        m_sumaVydelku += rent(pocetDni);
        sale();
    }

    void print(){
        cout << "Najeto: " << m_najetoKm << ", cena na den: " << m_cenaNaDen
        << ", celkovy vydelek: " << m_sumaVydelku << endl;
    }

private:
    void sale(){
        while (m_opotrebenie >= 10000){
            m_cenaNaDen *= 0.9;
            m_opotrebenie -= 10000;
        }
    }

};

    //cviko 4

class Potion{
    int m_health;
public:
    int getHealth(){
        return m_health;
    }
    Potion(int health){
        m_health = health;
    }

};

class Knight{
    int m_health;
public:
    Knight(int health){
        m_health = health;
    }

    int getHealthKnight(){
        return m_health;
    }

    void drinkPotion(Potion* potion){
        m_health += potion->getHealth();
        delete potion;
    }
};





int main() {
    //cviko 1

//    Planet *jupiter = new Planet(31780000000, 69911, 78.9);
//    jupiter->print();


    //cviko 2

//    Student *Nikolas = new Student("Nikolas Stastny", 9905083458, "Dubodiel 232, SR");
//    Student *Natalia = new Student("Natalia Holbova", 9924658458);
//    Nikolas->print();
//    Natalia->print();
//    Natalia->setBydlisko("Chotarna 14");
//    Natalia->print();

    //cviko 3

//    Auto *fiat = new Auto(125000, 40, 5600);
//    cout << "Nove: "; fiat->print();
//    cout << "60 dni prenajmu stoji: " << fiat->rent(60) << endl;
//    fiat->zapujceni(37051, 60);
//    cout << "po pozicani: "; fiat->print();

   // cviko 4

//Knight* Nikolasko = new Knight(100);
//Potion* lektvar = new Potion(15);
//cout << "Rytier ma aktualne: " << Nikolasko->getHealthKnight() << " zivotov" << endl;
//Nikolasko->drinkPotion(lektvar);
//cout << "Rytier ma po vypiti lektvaru: " << Nikolasko->getHealthKnight() << " zivotov" << endl;


    return 0;
}
