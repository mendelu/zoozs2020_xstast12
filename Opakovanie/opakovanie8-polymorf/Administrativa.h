//
// Created by Nick on 29. 11. 2020.
//

#ifndef OPAKOVANIE8_POLYMORF_ADMINISTRATIVA_H
#define OPAKOVANIE8_POLYMORF_ADMINISTRATIVA_H

#include "PracovnaPozicia.h"

class Administrativa: public PracovnaPozicia {
public:
    Administrativa(std::string jmeno, int pocetLet);

    int vratPlat();
};


#endif //OPAKOVANIE8_POLYMORF_ADMINISTRATIVA_H
