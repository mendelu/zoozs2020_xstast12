//
// Created by Nick on 29. 11. 2020.
//

#ifndef OPAKOVANIE8_POLYMORF_PROGRAMATOR_H
#define OPAKOVANIE8_POLYMORF_PROGRAMATOR_H

#include "PracovnaPozicia.h"
class Programator: public  PracovnaPozicia{
public:
    Programator(std::string jmeno, int pocetLet);

    int vratPlat();
};


#endif //OPAKOVANIE8_POLYMORF_PROGRAMATOR_H
