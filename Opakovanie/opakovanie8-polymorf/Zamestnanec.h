#ifndef OPAKOVANIE8_POLYMORF_ZAMESTNANEC_H
#define OPAKOVANIE8_POLYMORF_ZAMESTNANEC_H

#include <iostream>




class Zamestnanec {
protected:
    std::string m_jmeno;
    int pocetLetNaPozici;


public:
    Zamestnanec(std::string jmeno, int pocetLet);
    virtual int vratPlat()=0;

};


#endif //OPAKOVANIE8_POLYMORF_ZAMESTNANEC_H
