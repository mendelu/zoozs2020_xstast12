//
// Created by Nick on 29. 11. 2020.
//

#ifndef OPAKOVANIE8_POLYMORF_PRACOVNAPOZICIA_H
#define OPAKOVANIE8_POLYMORF_PRACOVNAPOZICIA_H

#include "Zamestnanec.h"
#include <iostream>




class PracovnaPozicia: public Zamestnanec {

public:
    PracovnaPozicia(std::string jmeno, int pocetLet);
    virtual int vratPlat()=0;
};


#endif //OPAKOVANIE8_POLYMORF_PRACOVNAPOZICIA_H
