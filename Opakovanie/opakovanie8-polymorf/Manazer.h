//
// Created by Nick on 29. 11. 2020.
//

#ifndef OPAKOVANIE8_POLYMORF_MANAZER_H
#define OPAKOVANIE8_POLYMORF_MANAZER_H
#include "PracovnaPozicia.h"

class Manazer: public PracovnaPozicia {
public:
    Manazer(std::string jmeno, int pocetLet);

    int vratPlat();
};


#endif //OPAKOVANIE8_POLYMORF_MANAZER_H
