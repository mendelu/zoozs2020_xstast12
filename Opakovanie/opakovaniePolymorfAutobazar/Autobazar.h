//
// Created by Nick on 30. 11. 2020.
//

#ifndef OPAKOVANIEPOLYMORFAUTOBAZAR_AUTOBAZAR_H
#define OPAKOVANIEPOLYMORFAUTOBAZAR_AUTOBAZAR_H

#include "vector"
#include "Auto.h"

class Autobazar {
    std::vector<Auto*> m_auta;
public:
    Autobazar();
    void vypisAuta();
    int getMajetek();
    void pridajAuto(Auto* automobil);
    void stocKM(Auto* automobil, int km);
};


#endif //OPAKOVANIEPOLYMORFAUTOBAZAR_AUTOBAZAR_H
