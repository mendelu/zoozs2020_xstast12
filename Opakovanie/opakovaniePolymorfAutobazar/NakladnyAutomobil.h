//
// Created by Nick on 30. 11. 2020.
//

#ifndef OPAKOVANIEPOLYMORFAUTOBAZAR_NAKLADNYAUTOMOBIL_H
#define OPAKOVANIEPOLYMORFAUTOBAZAR_NAKLADNYAUTOMOBIL_H

#include "Auto.h"

class NakladnyAutomobil: public Auto {
    int m_nosnnost;
public:
    NakladnyAutomobil(std::string model, int najeto, int povodnacena, int nosnost);
    int getCena();
    void printInfo();
};


#endif //OPAKOVANIEPOLYMORFAUTOBAZAR_NAKLADNYAUTOMOBIL_H
