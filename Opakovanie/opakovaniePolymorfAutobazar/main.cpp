#include <iostream>
#include "OsobnyAutomobil.h"
#include "NakladnyAutomobil.h"
#include "Autobazar.h"

int main() {
    Autobazar* bazar = new Autobazar();
    Auto* ford = new OsobnyAutomobil("ford", 237000, 250000, true);
    Auto* mazda = new OsobnyAutomobil("mazda", 50000, 400000, false);
    Auto* honda = new NakladnyAutomobil("honda", 400000, 1000000, 10);

    bazar->pridajAuto(ford);
    bazar->pridajAuto(mazda);
    bazar->pridajAuto(honda);

    bazar->vypisAuta();
    std::cout << "Celkovy majetok: " << bazar->getMajetek();




    return 0;
}
