//
// Created by Nick on 30. 11. 2020.
//

#include "Autobazar.h"

Autobazar::Autobazar() = default;

void Autobazar::stocKM(Auto* automobil,int km) {
    automobil->stocKM(km);
}

void Autobazar::pridajAuto(Auto *automobil) {
    m_auta.push_back(automobil);
}

void Autobazar::vypisAuta() {
    for(auto automobil:m_auta){
        automobil->printInfo();
        std::cout << "-----------------" << std::endl;
    }
}

int Autobazar::getMajetek() {
    int majetek=0;
    for(auto automobil:m_auta){
        majetek += automobil->getCena();
    }
    return majetek;
}
