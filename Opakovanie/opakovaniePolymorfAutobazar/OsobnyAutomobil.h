//
// Created by Nick on 30. 11. 2020.
//

#ifndef OPAKOVANIEPOLYMORFAUTOBAZAR_OSOBNYAUTOMOBIL_H
#define OPAKOVANIEPOLYMORFAUTOBAZAR_OSOBNYAUTOMOBIL_H

#include "Auto.h"

class OsobnyAutomobil: public Auto {
    bool m_bourane;
public:
    OsobnyAutomobil(std::string model, int najeto, int povodnaCena, bool bourane);
    int getCena();
    void printInfo();

};


#endif //OPAKOVANIEPOLYMORFAUTOBAZAR_OSOBNYAUTOMOBIL_H
