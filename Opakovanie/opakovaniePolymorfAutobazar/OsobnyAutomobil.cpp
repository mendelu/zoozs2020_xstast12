//
// Created by Nick on 30. 11. 2020.
//

#include "OsobnyAutomobil.h"

OsobnyAutomobil::OsobnyAutomobil(std::string model, int najeto, int povodnaCena, bool bourane):Auto(model, najeto, povodnaCena){
    m_bourane = bourane;
}

void OsobnyAutomobil::printInfo() {
    std::cout << "Auto Osobne model: " << m_model << ", najeto: " << m_najetoKm <<
    ", PC: " << m_povodnaCenaAuta << ", bourane?: " << m_bourane <<std::endl;
}

int OsobnyAutomobil::getCena() {
    return  ((1-(m_najetoKm/300000))*m_povodnaCenaAuta*(1-0.5*m_bourane));
}

