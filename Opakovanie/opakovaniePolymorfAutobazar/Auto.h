//
// Created by Nick on 30. 11. 2020.
//

#ifndef OPAKOVANIEPOLYMORFAUTOBAZAR_AUTO_H
#define OPAKOVANIEPOLYMORFAUTOBAZAR_AUTO_H

#include <iostream>

class Auto {
protected:
    std::string m_model;
    int m_najetoKm;
    int m_povodnaCenaAuta;

public:
    Auto(std::string model, int najetokm, int povodnacena);
    void stocKM(int pocet);
    virtual int getCena()=0;
    virtual void printInfo()=0;

};


#endif //OPAKOVANIEPOLYMORFAUTOBAZAR_AUTO_H
