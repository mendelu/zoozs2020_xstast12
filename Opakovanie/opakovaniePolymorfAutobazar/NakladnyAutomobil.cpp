//
// Created by Nick on 30. 11. 2020.
//

#include "NakladnyAutomobil.h"

NakladnyAutomobil::NakladnyAutomobil(std::string model, int najeto, int povodnacena, int nosnost):Auto(model, najeto, povodnacena) {
    m_nosnnost = nosnost;
}

int NakladnyAutomobil::getCena() {
    return ((1-(m_najetoKm/500000))*m_povodnaCenaAuta);
}

void NakladnyAutomobil::printInfo() {
    std::cout << "Auto Nakladne model: " << m_model << ", najeto: " << m_najetoKm <<
              ", PC: " << m_povodnaCenaAuta << ", nosnost: " << m_nosnnost <<std::endl;

}
