//
// Created by Nick on 29. 11. 2020.
//

#include "Budova.h"

Budova::Budova(int naklady, std::string nazov) {
    m_nazov =nazov;
    m_nakladyNaPostavenie = naklady;
}

int Budova::getNaklady() {
    return m_nakladyNaPostavenie;
}

void Budova::vypisBudovu() {
    std::cout << "Nazov: " << m_nazov << ", naklady na budovu: " << m_nakladyNaPostavenie << std::endl;
}