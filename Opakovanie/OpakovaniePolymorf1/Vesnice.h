#ifndef OPAKOVANIEPOLYMORF1_VESNICE_H
#define OPAKOVANIEPOLYMORF1_VESNICE_H

#include "KralovskaRada.h"
#include <vector>
#include <iostream>
#include "Budova.h"
#include "Kovarna.h"
#include "Dom.h"

class Vesnice {
    int bohatstvi;
    int pocetBudov;
    std::vector<Budova*> m_budovy;
    KralovskaRada* kralovskaRada;
public:
    Vesnice(int kapital, std::string menoKrala);
    void postavDom(int pocetObyvatelov);
    void postavKovarnu();
    void zburajBudovu();
    void printInfo();
    ~Vesnice();


};


#endif //OPAKOVANIEPOLYMORF1_VESNICE_H
