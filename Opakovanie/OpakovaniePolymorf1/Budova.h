//
// Created by Nick on 29. 11. 2020.
//

#ifndef OPAKOVANIEPOLYMORF1_BUDOVA_H
#define OPAKOVANIEPOLYMORF1_BUDOVA_H

#include <iostream>

class Budova {
protected:
    int m_nakladyNaPostavenie;
    std::string m_nazov;
public:
    Budova(int naklady, std::string nazov);
    int getNaklady();
    void vypisBudovu();
};


#endif //OPAKOVANIEPOLYMORF1_BUDOVA_H
