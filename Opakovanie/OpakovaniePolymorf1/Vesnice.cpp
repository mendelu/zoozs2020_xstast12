//
// Created by Nick on 29. 11. 2020.
//

#include "Vesnice.h"

Vesnice::Vesnice(int kapital, std::string menoKrala) {
    pocetBudov = 0;
    kralovskaRada = new KralovskaRada(menoKrala);
    bohatstvi = kapital;
}

void Vesnice::postavKovarnu() {
    if ((bohatstvi >= 150) && (pocetBudov < 5)){
        Budova* budova = new Kovarna();
        m_budovy.push_back(budova);
        bohatstvi -= 150;
        pocetBudov++;
    } else{
        std::cout << "Mas malo penazi alebo uz mas 5 stavieb.";
    }
}

void Vesnice::postavDom(int pocetObyvatelov) {
    if ((bohatstvi >= pocetObyvatelov*25) && (pocetBudov < 5)){
        Budova* budova = new Dom(pocetObyvatelov);
        m_budovy.push_back(budova);
        bohatstvi -= pocetObyvatelov*25;
        pocetBudov++;
    } else{
        std::cout << "Mas malo penazi alebo uz mas 5 stavieb.";
    }

}

void Vesnice::zburajBudovu() {
    m_budovy.pop_back();
}

void Vesnice::printInfo() {
    std::cout << "Dedina ma krala: " << kralovskaRada->getKral()<< std::endl;
    std::cout << "Budovy: " << std::endl;
    for (auto budova:m_budovy) {
        budova->vypisBudovu();
        std::cout << "-----------" << std::endl;
    }
}

Vesnice::~Vesnice() {
    delete kralovskaRada;
}
