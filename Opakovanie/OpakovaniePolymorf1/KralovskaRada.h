//
// Created by Nick on 29. 11. 2020.
//

#ifndef OPAKOVANIEPOLYMORF1_KRALOVSKARADA_H
#define OPAKOVANIEPOLYMORF1_KRALOVSKARADA_H

#include <iostream>
class KralovskaRada {
    std::string m_kral;
public:
    KralovskaRada(std::string menoKrala);
    std::string getKral();
    void zmenKrala(std::string meno);
};


#endif //OPAKOVANIEPOLYMORF1_KRALOVSKARADA_H
