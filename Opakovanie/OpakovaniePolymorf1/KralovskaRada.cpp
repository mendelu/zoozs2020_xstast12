#include "KralovskaRada.h"

KralovskaRada::KralovskaRada(std::string menoKrala) {
    m_kral = menoKrala;
}

std::string KralovskaRada::getKral() {return m_kral;}

void KralovskaRada::zmenKrala(std::string meno) {m_kral = meno;}