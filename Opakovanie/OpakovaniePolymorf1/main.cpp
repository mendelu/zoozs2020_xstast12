#include <iostream>
#include "Vesnice.h"

int main() {
    Vesnice* dubodiel = new Vesnice(1000, "Nikolas");
    dubodiel->postavKovarnu();
    dubodiel->postavDom(3);
    dubodiel->postavDom(2);
    dubodiel->postavDom(7);

    dubodiel->printInfo();

    std::cout << "========================\nIdeme zburat budovau: \n=============================" << std::endl;
    dubodiel->zburajBudovu();
    dubodiel->printInfo();
    return 0;

}
