#include <iostream>

#include "Letisko/Letisko.h"
#include "Letisko/NakladneLietadlo.h"
#include "Letisko/DopravneLietadlo.h"

#include "PCTovaren/IntelTovaren.h"
#include "PCTovaren/AMDTovaren.h"


int main() {

    /// ---Prva cast
    /*

    Letisko* letisko = new Letisko("Brno", 3);

    Lietadlo* let1 = new NakladneLietadlo(20, "Nakladne 1", 50);
    Lietadlo* let2 = new NakladneLietadlo(10, "Nakladne 2", 60);
    Lietadlo* let4 = new DopravneLietadlo(50, "Dopravne 1", 30);
    Lietadlo* let3 = new DopravneLietadlo(5, "Dopravne 2", 100);

    letisko->pridaj(let1);
    letisko->pridaj(let2);
    letisko->pridaj(let3);
    letisko->pridaj(let4);

   letisko->printInfo();

   letisko->odober(let1);
   letisko->odober(let3);

   letisko->printInfo();

    delete letisko;
    */

    /// ---Druha cast

    AMDTovaren* amd = new AMDTovaren();
    IntelTovaren* intel = new IntelTovaren();

    CPU* intelcpu = intel->getCPU();
    Disk* intelDisk = intel->getDisk();
    std::cout << "Intel Cpu rychlost " << intelcpu->getVykon() << ", and disk info: " << intelDisk->getRychlost() << std::endl;

    CPU* amdcpu = amd->getCPU();
    Disk* amdDisk = amd->getDisk();
    std::cout << "AMD Cpu rychlost " << amdcpu->getVykon() << ", and disk info: " << amdDisk->getRychlost() << std::endl;

    return 0;
}
