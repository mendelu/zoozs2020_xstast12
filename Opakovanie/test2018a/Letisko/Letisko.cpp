//
// Created by Nick on 2. 1. 2021.
//

#include "Letisko.h"

Letisko::Letisko(std::string nazov, int kapacita) {
    m_nazov = nazov;
    m_kapacita = kapacita;
}

void Letisko::pridaj(Lietadlo *lietadlo) {
    if (m_lietadla.size() < m_kapacita){
        m_lietadla.push_back(lietadlo);
    } else{
        std::cout << "Plna kapacita, nemozno pridat lietadlo\n";
    }
}

void Letisko::odober(Lietadlo *lietadlo) {
    for (int i = 0; i < m_lietadla.size(); i++) {
        if (lietadlo == m_lietadla.at(i)){
            m_lietadla.erase(m_lietadla.begin()+i);
            delete lietadlo;
            break;

        }
    }



}

void Letisko::printInfo() {
    std::cout << "Lietadla na letisku " << m_nazov << " s kapacitou: " << m_kapacita << std::endl;
    std::cout << "++++++++++++++\n";
    for (auto lietadlo:m_lietadla) {
        lietadlo->printInfo();
        std::cout << std::endl;
    }
    std::cout << "++++++++++++++\n";
}

Letisko::~Letisko() {
    for (auto letadlo:m_lietadla) {
        delete letadlo;
    }

}

