//
// Created by Nick on 2. 1. 2021.
//

#ifndef TEST2018A_NAKLADNELIETADLO_H
#define TEST2018A_NAKLADNELIETADLO_H
#include "Lietadlo.h"

class NakladneLietadlo: public Lietadlo {
    int m_vahaZavazadlovehoProstoru;
public:
    NakladneLietadlo(int spotreba, std::string oznaceine, int vahaZavazadlovehoProstoru);
    int getSpotreba();
    void printInfo();
};


#endif //TEST2018A_NAKLADNELIETADLO_H
