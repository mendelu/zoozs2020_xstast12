//
// Created by Nick on 2. 1. 2021.
//

#include "NakladneLietadlo.h"

int NakladneLietadlo::getSpotreba(){return m_spotreba * (m_vahaZavazadlovehoProstoru/30);}


void NakladneLietadlo::printInfo(){
    std::cout << "Nakladne lietadlo oznacenie: " << m_oznacenie << std::endl;
    std::cout << "Spotreba: " << getSpotreba() << ", vaha zavazadloveho prostoru: "
    << m_vahaZavazadlovehoProstoru << std::endl;
}

NakladneLietadlo::NakladneLietadlo(int spotreba, std::string oznaceine, int vahaZavazadlovehoProstoru):Lietadlo(spotreba, oznaceine) {
    m_vahaZavazadlovehoProstoru = vahaZavazadlovehoProstoru;
}
