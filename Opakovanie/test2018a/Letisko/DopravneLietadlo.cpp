//
// Created by Nick on 2. 1. 2021.
//

#include "DopravneLietadlo.h"

DopravneLietadlo::DopravneLietadlo(int spotreba, std::string oznaceine, int pocetPasazeru):Lietadlo(spotreba,oznaceine) {
    m_pocetPasazeru = pocetPasazeru;
}

int DopravneLietadlo::getSpotreba(){return (m_pocetPasazeru*90)/m_spotreba;}


void DopravneLietadlo::printInfo(){
    std::cout << "Dopravne lietadlo oznacenie: " << m_oznacenie << std::endl;
    std::cout << "Spotreba: " << getSpotreba() << ", pocet pasazierov: " << m_pocetPasazeru << std::endl;
}