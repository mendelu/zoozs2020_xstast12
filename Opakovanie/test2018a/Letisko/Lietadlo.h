//
// Created by Nick on 2. 1. 2021.
//

#ifndef TEST2018A_LIETADLO_H
#define TEST2018A_LIETADLO_H

#include "iostream"


class Lietadlo {
protected:
    int m_spotreba;
    std::string m_oznacenie;
public:
    Lietadlo(int spotreba, std::string oznaceine);
    virtual int getSpotreba() = 0;
    virtual void printInfo() = 0;
};


#endif //TEST2018A_LIETADLO_H
