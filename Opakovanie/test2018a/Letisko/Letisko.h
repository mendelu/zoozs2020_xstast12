//
// Created by Nick on 2. 1. 2021.
//

#ifndef TEST2018A_LETISKO_H
#define TEST2018A_LETISKO_H

#include <iostream>
#include <vector>
#include "Lietadlo.h"


class Letisko {
    std::string m_nazov;
    int m_kapacita;
    std::vector<Lietadlo*> m_lietadla;

public:
    Letisko(std::string nazov, int kapacita);
    void pridaj(Lietadlo* lietadlo);
    void odober(Lietadlo* lietadlo);
    void printInfo();
    ~Letisko();

};


#endif //TEST2018A_LETISKO_H
