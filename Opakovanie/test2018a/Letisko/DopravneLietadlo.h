//
// Created by Nick on 2. 1. 2021.
//

#ifndef TEST2018A_DOPRAVNELIETADLO_H
#define TEST2018A_DOPRAVNELIETADLO_H
#include <iostream>
#include "Lietadlo.h"

class DopravneLietadlo: public Lietadlo {
    int m_pocetPasazeru;
public:
    DopravneLietadlo(int spotreba, std::string oznaceine, int pocetPasazeru);
    int getSpotreba();
    void printInfo();

};


#endif //TEST2018A_DOPRAVNELIETADLO_H
