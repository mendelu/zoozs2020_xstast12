//
// Created by Nick on 2. 1. 2021.
//

#ifndef TEST2018A_SSD_H
#define TEST2018A_SSD_H
#include "Disk.h"

class SSD: public Disk {
    int m_rychlostZapisu;
    int m_hloubkaZapisu;
public:
    SSD();
    int getRychlost();
};


#endif //TEST2018A_SSD_H
