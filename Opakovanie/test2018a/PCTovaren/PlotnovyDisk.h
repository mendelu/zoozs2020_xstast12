//
// Created by Nick on 2. 1. 2021.
//

#ifndef TEST2018A_PLOTNOVYDISK_H
#define TEST2018A_PLOTNOVYDISK_H
#include "Disk.h"

class PlotnovyDisk: public Disk {
    int m_rychlostZapisu;
public:
    PlotnovyDisk();
    int getRychlost();
};


#endif //TEST2018A_PLOTNOVYDISK_H
