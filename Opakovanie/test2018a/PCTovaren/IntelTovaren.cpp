//
// Created by Nick on 2. 1. 2021.
//

#include "IntelTovaren.h"

CPU *IntelTovaren::getCPU() {
    return new Intel();
}

Disk *IntelTovaren::getDisk() {
    return new SSD();
}
