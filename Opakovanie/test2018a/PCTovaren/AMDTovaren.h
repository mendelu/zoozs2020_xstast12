//
// Created by Nick on 2. 1. 2021.
//

#ifndef TEST2018A_AMDTOVAREN_H
#define TEST2018A_AMDTOVAREN_H
#include "AMD.h"
#include "PlotnovyDisk.h"

class AMDTovaren {
public:
    CPU* getCPU();
    Disk* getDisk();
};


#endif //TEST2018A_AMDTOVAREN_H
