//
// Created by Nick on 2. 1. 2021.
//

#ifndef TEST2018A_INTELTOVAREN_H
#define TEST2018A_INTELTOVAREN_H
#include "Intel.h"
#include "SSD.h"

class IntelTovaren {
public:
    CPU* getCPU();
    Disk* getDisk();
};


#endif //TEST2018A_INTELTOVAREN_H
