//
// Created by Nick on 2. 1. 2021.
//

#ifndef TEST2018A_INTEL_H
#define TEST2018A_INTEL_H
#include "CPU.h"

class Intel: public CPU {
    int m_frekvence;
public:
    Intel();
    int getVykon();
};


#endif //TEST2018A_INTEL_H
