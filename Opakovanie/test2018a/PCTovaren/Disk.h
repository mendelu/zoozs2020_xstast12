//
// Created by Nick on 2. 1. 2021.
//

#ifndef TEST2018A_DISK_H
#define TEST2018A_DISK_H


class Disk {
public:
    virtual int getRychlost() = 0;
};


#endif //TEST2018A_DISK_H
