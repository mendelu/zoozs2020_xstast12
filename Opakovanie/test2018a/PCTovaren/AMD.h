//
// Created by Nick on 2. 1. 2021.
//

#ifndef TEST2018A_AMD_H
#define TEST2018A_AMD_H
#include "CPU.h"

class AMD:public CPU {
    int m_frekvence;
    int m_cache;
public:
    AMD();
    int getVykon();
};


#endif //TEST2018A_AMD_H
