#include "NakladneAuto.h"


NakladneAuto::NakladneAuto(int vaha, int vykonMotoru, int vahaMotoru, int vahaNavesu)
:Auto(vaha, vykonMotoru, vahaMotoru) {
    if (vahaNavesu > 0){
        naves = new Naves(vahaNavesu);
    }
}

void NakladneAuto::printInfo(){
    std::cout << "Nakladne auto s vahou: "  << m_vaha;
    std::cout << "Motor vykon: " << m_motor->getVykon() << ", vaha motora: " << m_motor->getVaha() << std::endl;
    std::cout << "Naprava vaha: " << naves->getVaha() << std::endl;
}

NakladneAuto::~NakladneAuto(){
    delete naves;
}