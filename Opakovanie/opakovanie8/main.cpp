#include <iostream>
#include "Auto.h"
#include "Osobak.h"
#include "NakladneAuto.h"

int main() {

    Auto* osobne = new Osobak(100, 100 ,100);
    Auto* nakladne = new NakladneAuto(500,500,500, 10);

    nakladne->printInfo();

    return 0;
}
