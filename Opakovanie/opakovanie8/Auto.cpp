#include "Auto.h"


Auto::Auto(int vaha, int vykonMotoru, int vahaMotoru){
    m_vaha = vaha;
    m_motor = new Motor(vykonMotoru, vahaMotoru);
}
void Auto::printInfo(){
    std::cout << "Auto s vahou: "  << m_vaha;
    std::cout << "Motor vykon: " << m_motor->getVykon() << ", vaha motora: " << m_motor->getVaha() << std::endl;

}
Auto::~Auto(){
    delete m_motor;
}