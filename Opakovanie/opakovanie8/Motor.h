//
// Created by Nick on 28. 11. 2020.
//

#ifndef OPAKOVANIE8_MOTOR_H
#define OPAKOVANIE8_MOTOR_H


class Motor {
private:
    int m_vykon;
    int m_vaha;
public:
    Motor(int vykon, int vaha);
    int getVaha();
    int getVykon();

};


#endif //OPAKOVANIE8_MOTOR_H
