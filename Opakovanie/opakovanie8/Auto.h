//
// Created by Nick on 28. 11. 2020.
//

#ifndef OPAKOVANIE8_AUTO_H
#define OPAKOVANIE8_AUTO_H

#include "Motor.h"
#include <iostream>


class Auto {
protected:
    int m_vaha;
    Motor* m_motor;
    Auto(int vaha, int vykonMotoru, int vahaMotoru);
public:

    virtual void printInfo();
    ~Auto();
};


#endif //OPAKOVANIE8_AUTO_H
