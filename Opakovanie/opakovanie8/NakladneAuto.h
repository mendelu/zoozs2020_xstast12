#ifndef OPAKOVANIE8_NAKLADNEAUTO_H
#define OPAKOVANIE8_NAKLADNEAUTO_H


#include "Auto.h"
#include "Naves.h"

class NakladneAuto:public Auto{
protected:
    Naves* naves;
public:
    NakladneAuto(int vaha, int vykonMotoru, int vahaMotoru, int vahaNavesu);
    void printInfo();
    ~NakladneAuto();
};


#endif //OPAKOVANIE8_NAKLADNEAUTO_H
