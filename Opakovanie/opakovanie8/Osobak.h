#ifndef OPAKOVANIE8_OSOBAK_H
#define OPAKOVANIE8_OSOBAK_H

#include "Auto.h"

class Osobak:public Auto {
public:
    Osobak(int vaha, int vykonMotoru, int vahaMotoru);
    void printInfo();

};


#endif //OPAKOVANIE8_OSOBAK_H
