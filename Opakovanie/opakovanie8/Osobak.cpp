//
// Created by Nick on 28. 11. 2020.
//

#include "Osobak.h"


Osobak::Osobak(int vaha, int vykonMotoru, int vahaMotoru):Auto(vaha, vykonMotoru, vahaMotoru){}

void Osobak::printInfo() {
    std::cout << "Osobne auto s vahou: " << m_vaha;
    std::cout << "Motor vykon: " << m_motor->getVykon() << ", vaha motora: " << m_motor->getVaha() << std::endl;
}