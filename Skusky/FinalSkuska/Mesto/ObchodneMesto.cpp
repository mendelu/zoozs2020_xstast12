//
// Created by Nick on 4. 1. 2021.
//

#include "ObchodneMesto.h"

void ObchodneMesto::buildMesto() {
    m_mesto->setKomfort(2);
    m_mesto->setCenaBydleni(2);
    m_mesto->addBudova(new Budova("Trziste", 0, 5));
    m_mesto->addBudova(new Budova("Obchod", 5, 0));
}
