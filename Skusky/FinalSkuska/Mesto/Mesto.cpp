//
// Created by Nick on 4. 1. 2021.
//

#include "Mesto.h"

Mesto::Mesto(std::string jmeno, int pocStavUspor) {
    m_jmeno = jmeno;
    m_stavUspor = pocStavUspor;
    m_cenaBydleni = 0;
    m_komfort = 0;
}

std::string Mesto::getJmeno() {
    return m_jmeno;
}

int Mesto::getCenaBydleni() {
    int cena = m_cenaBydleni;
    for (auto budova:m_budovy) {
        cena += budova->getBonusCenA();
    }
    return cena;
}

int Mesto::getKomfort() {
    int komfort = m_komfort;
    for (auto budova:m_budovy) {
        komfort += budova->getBonuskomf();
    }
    return komfort;


}
int Mesto::getStavUspor() {return m_stavUspor;}

void Mesto::setKomfort(int hodnota) {
    m_komfort = hodnota;
}
void Mesto::setCenaBydleni(int hodnota) {m_cenaBydleni = hodnota;}

void Mesto::addBudova(Budova *budova) {
    m_budovy.push_back(budova);
}

Mesto::~Mesto() {
    for (auto budova:m_budovy) {
        delete budova;
    }
}

void Mesto::printInfo() {
    std::cout << "Mesto: " << m_jmeno << " komfort: " << getKomfort()
    << ", cenaBydleni: " << getCenaBydleni() << std::endl;
}
