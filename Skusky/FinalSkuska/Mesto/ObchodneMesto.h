//
// Created by Nick on 4. 1. 2021.
//

#ifndef FINALSKUSKA_OBCHODNEMESTO_H
#define FINALSKUSKA_OBCHODNEMESTO_H
#include "MestoBuilder.h"

class ObchodneMesto: public MestoBuilder {
public:
    void buildMesto();
};


#endif //FINALSKUSKA_OBCHODNEMESTO_H
