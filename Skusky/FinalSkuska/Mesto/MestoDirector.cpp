//
// Created by Nick on 4. 1. 2021.
//

#include "MestoDirector.h"

MestoDirector::MestoDirector(MestoBuilder *builder) {
    m_mestoBuilder = builder;
}
void MestoDirector::setMestoBuilder(MestoBuilder *builder) {m_mestoBuilder = builder;}
Mesto* MestoDirector::constructMesto(std::string jmeno, int pociatocnyStav) {
    m_mestoBuilder->createNewMesto(jmeno, pociatocnyStav);
    m_mestoBuilder->buildMesto();
    return m_mestoBuilder->getMesto();
}