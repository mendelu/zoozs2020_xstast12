//
// Created by Nick on 4. 1. 2021.
//

#ifndef FINALSKUSKA_MESTOBUILDER_H
#define FINALSKUSKA_MESTOBUILDER_H
#include "Mesto.h"

class MestoBuilder {
protected:
    Mesto* m_mesto;
public:
    MestoBuilder();
    void createNewMesto(std::string jmeno, int pociatocnystav);
    Mesto* getMesto();
    virtual void buildMesto()=0;
};


#endif //FINALSKUSKA_MESTOBUILDER_H
