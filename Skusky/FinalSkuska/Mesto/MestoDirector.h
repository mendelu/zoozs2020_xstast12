//
// Created by Nick on 4. 1. 2021.
//

#ifndef FINALSKUSKA_MESTODIRECTOR_H
#define FINALSKUSKA_MESTODIRECTOR_H
#include "MestoBuilder.h"
#include <iostream>

class MestoDirector {
    MestoBuilder* m_mestoBuilder;
public:
    MestoDirector(MestoBuilder* builder);
    void setMestoBuilder(MestoBuilder* builder);
    Mesto* constructMesto(std::string jmeno, int pociatocnyStav);
};


#endif //FINALSKUSKA_MESTODIRECTOR_H
