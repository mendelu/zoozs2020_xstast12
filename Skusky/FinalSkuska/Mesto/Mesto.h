//
// Created by Nick on 4. 1. 2021.
//

#ifndef FINALSKUSKA_MESTO_H
#define FINALSKUSKA_MESTO_H
#include <iostream>
#include "Budova.h"
#include <vector>

class Mesto {
    std::string m_jmeno;
    int m_stavUspor;
    int m_komfort;
    int m_cenaBydleni;
    std::vector<Budova*> m_budovy;
public:
    Mesto(std::string jmeno, int pocStavUspor);
    std::string getJmeno();
    int getStavUspor();
    int getKomfort();
    int getCenaBydleni();
    void setKomfort(int hodnota);
    void setCenaBydleni(int hodnota);
    void addBudova(Budova* budova);

    void printInfo();

    ~Mesto();
};


#endif //FINALSKUSKA_MESTO_H
