//
// Created by Nick on 4. 1. 2021.
//

#include "AlchymistickeMesto.h"

void AlchymistickeMesto::buildMesto() {
    m_mesto->setKomfort(3);
    m_mesto->setCenaBydleni(3);
    m_mesto->addBudova(new Budova("Laborator", 0, 10));
    m_mesto->addBudova(new Budova("Magicka Studna", 10, 0));
}
