//
// Created by Nick on 4. 1. 2021.
//

#include "MestoBuilder.h"

MestoBuilder::MestoBuilder() {
    m_mesto = nullptr;
}

Mesto *MestoBuilder::getMesto() {
    return m_mesto;
}


void MestoBuilder::createNewMesto(std::string jmeno, int pociatocnystav) {
    m_mesto = new Mesto(jmeno, pociatocnystav);
}
