//
// Created by Nick on 4. 1. 2021.
//

#ifndef FINALSKUSKA_DULNIMESTO_H
#define FINALSKUSKA_DULNIMESTO_H
#include "MestoBuilder.h"

class DulniMesto: public MestoBuilder {
public:
    void buildMesto();
};


#endif //FINALSKUSKA_DULNIMESTO_H
