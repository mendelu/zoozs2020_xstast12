//
// Created by Nick on 4. 1. 2021.
//

#ifndef FINALSKUSKA_BUDOVA_H
#define FINALSKUSKA_BUDOVA_H
#include <iostream>


class Budova {
    std::string m_jmeno;
    int m_bonusKomfort;
    int m_bonusCenabydleni;
public:
    Budova(std::string jmeno, int bonusKomf, int bonusCena);
    int getBonuskomf();
    int getBonusCenA();
};


#endif //FINALSKUSKA_BUDOVA_H
