//
// Created by Nick on 4. 1. 2021.
//

#ifndef FINALSKUSKA_ALCHYMISTICKEMESTO_H
#define FINALSKUSKA_ALCHYMISTICKEMESTO_H
#include "MestoBuilder.h"

class AlchymistickeMesto: public MestoBuilder {
public:
    void buildMesto();
};


#endif //FINALSKUSKA_ALCHYMISTICKEMESTO_H
