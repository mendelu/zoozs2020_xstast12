//
// Created by Nick on 4. 1. 2021.
//

#include "Budova.h"

Budova::Budova(std::string jmeno, int bonusKomf, int bonusCena) {
    m_jmeno = jmeno;
    m_bonusKomfort = bonusKomf;
    m_bonusCenabydleni = bonusCena;
}

int Budova::getBonuskomf() {
    return m_bonusKomfort;
}

int Budova::getBonusCenA() {
    return m_bonusCenabydleni;
}
