//
// Created by Nick on 4. 1. 2021.
//

#include "DulniMesto.h"

void DulniMesto::buildMesto() {
    m_mesto->setKomfort(1);
    m_mesto->setCenaBydleni(1);
    m_mesto->addBudova(new Budova("Dul", 0, 2));
    m_mesto->addBudova(new Budova("Obchod", 2, 0));
}
