//
// Created by Nick on 4. 1. 2021.
//

#ifndef FINALSKUSKA_UCETNICTVI_H
#define FINALSKUSKA_UCETNICTVI_H
#include "Dokument.h"
#include <vector>

class Ucetnictvi {
    std::vector<Dokument*> m_dokumety;
public:
    Ucetnictvi(){};
    void printInfoVyrizujeOsoba(std::string osoba);
    int scitajSumy();
    void pridajDokument(Dokument* dokument);
    ~Ucetnictvi();
};


#endif //FINALSKUSKA_UCETNICTVI_H
