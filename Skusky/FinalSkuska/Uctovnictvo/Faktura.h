//
// Created by Nick on 4. 1. 2021.
//

#ifndef FINALSKUSKA_FAKTURA_H
#define FINALSKUSKA_FAKTURA_H
#include "Dokument.h"

class Faktura: public Dokument {
    std::string m_ucet;
public:
    Faktura(std::string firma, std::string  datum, std::string osoba, int ciastka, std::string ucet);
    void tiskni();

};


#endif //FINALSKUSKA_FAKTURA_H
