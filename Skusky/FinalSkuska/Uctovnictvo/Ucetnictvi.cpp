//
// Created by Nick on 4. 1. 2021.
//

#include "Ucetnictvi.h"

void Ucetnictvi::printInfoVyrizujeOsoba(std::string osoba) {
    std::cout << osoba << " vybavuje nasledujuce dokumenty:\n";
    for(auto dokument:m_dokumety){
        if (dokument->getOsoba() == osoba){
            dokument->tiskni();
        }
    }
    std::cout << "--koniec vypisu dokumentov od osoba: \n" << osoba << std::endl;
}

int Ucetnictvi::scitajSumy() {
    int suma = 0;
    for(auto dokument:m_dokumety){
        suma+= dokument->getCiastka();
    }
    return suma;
}

void Ucetnictvi::pridajDokument(Dokument *dokument) {
    m_dokumety.push_back(dokument);
}

Ucetnictvi::~Ucetnictvi() {
    for(auto dokument:m_dokumety){
        delete dokument;
    }
}
