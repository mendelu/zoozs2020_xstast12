//
// Created by Nick on 4. 1. 2021.
//

#include "Objednavka.h"

Objednavka::Objednavka(std::string firma, std::string datum, std::string osoba, int ciastka, std::string adresa):Dokument(firma, datum, osoba, ciastka) {
    m_adresa = adresa;
}

void Objednavka::tiskni() {
    std::cout << "Objednavka od " <<m_firma << ", datum: " <<m_datum
              <<", vyrizuje: " << m_osoba << ", na ciastku " << m_ciastka << ", na adresu " << m_adresa << std::endl;
}

