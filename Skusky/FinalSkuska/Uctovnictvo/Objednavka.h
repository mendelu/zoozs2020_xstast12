//
// Created by Nick on 4. 1. 2021.
//

#ifndef FINALSKUSKA_OBJEDNAVKA_H
#define FINALSKUSKA_OBJEDNAVKA_H
#include "Dokument.h"

class Objednavka: public Dokument {
    std::string m_adresa;
public:
    Objednavka(std::string firma, std::string datum, std::string osoba, int ciastka, std::string adresa);
    void tiskni();
};


#endif //FINALSKUSKA_OBJEDNAVKA_H
