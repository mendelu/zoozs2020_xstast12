//
// Created by Nick on 4. 1. 2021.
//

#include "Dokument.h"

Dokument::Dokument(std::string firma, std::string datum, std::string osoba, int ciastka) {
    m_firma = firma;
    m_datum = datum;
    m_osoba = osoba;
    m_ciastka = ciastka;
}

void Dokument::zmenVyrizujiciOsobu(std::string osoba) {
    m_osoba = osoba;
}

std::string Dokument::getOsoba() {
    return m_osoba;
}

int Dokument::getCiastka() {
    return m_ciastka;
}
