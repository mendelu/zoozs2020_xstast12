//
// Created by Nick on 4. 1. 2021.
//

#include "Faktura.h"

Faktura::Faktura(std::string firma, std::string datum, std::string osoba, int ciastka, std::string ucet)
    :Dokument(firma, datum, osoba, ciastka) {
    m_ucet = ucet;
}

void Faktura::tiskni() {
    std::cout << "Faktura od " <<m_firma << ", datum: " <<m_datum
    <<", vyrizuje: " << m_osoba << ", na ciastku " << m_ciastka << ", na ucet " << m_ucet << std::endl;
}

