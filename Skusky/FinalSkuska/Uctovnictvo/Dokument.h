//
// Created by Nick on 4. 1. 2021.
//

#ifndef FINALSKUSKA_DOKUMENT_H
#define FINALSKUSKA_DOKUMENT_H
#include <iostream>

class Dokument {
protected:
    std::string m_firma;
    std::string m_datum;
    std::string m_osoba;
    int m_ciastka;
public:
    Dokument(std::string firma, std::string  datum, std::string osoba, int ciastka);
    virtual void tiskni() = 0;
    void zmenVyrizujiciOsobu(std::string osoba);
    std::string getOsoba();
    int getCiastka();
};


#endif //FINALSKUSKA_DOKUMENT_H
