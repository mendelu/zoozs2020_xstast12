#include <iostream>
#include "Mesto/MestoDirector.h"
#include "Mesto/DulniMesto.h"
#include "Mesto/ObchodneMesto.h"
#include "Mesto/AlchymistickeMesto.h"

#include "Uctovnictvo/Ucetnictvi.h"
#include "Uctovnictvo/Faktura.h"
#include "Uctovnictvo/Objednavka.h"


int main() {

    ///  --- Prva cast MESTO

    MestoDirector* director = new MestoDirector(new DulniMesto());
    Mesto* dulniMesto = director->constructMesto("Dulne mesto", 100);
    dulniMesto->printInfo();

    director->setMestoBuilder(new AlchymistickeMesto());
    Mesto* alchymMesto = director->constructMesto("alchym", 500);
    alchymMesto->printInfo();

    delete director;
    delete dulniMesto;
    delete alchymMesto;


    
    
    ///  --- Druha cast UCTOVNICTVO
    /*
    Ucetnictvi* ucto = new Ucetnictvi();
    ucto->pridajDokument(new Faktura("Fak1", "1.1.2021", "Nikolas", 15, "5418416349"));
    ucto->pridajDokument(new Objednavka("Obj1", "4.1.2021", "Nikolas", 15, "5349"));
    ucto->pridajDokument(new Faktura("Fak2", "15.1.2021", "Jozef", 300, "5416349"));
    ucto->pridajDokument(new Faktura("Fak3", "1.2.2021", "Nikolas", 15, "5418349"));

    ucto->printInfoVyrizujeOsoba("Nikolas");
    

    std::cout << "(Bolo mi povedane ze mam scitat sumy na objednavkach a fakturach)\n"
                 "Suma na objednavkach a fakturach je: " << ucto->scitajSumy() << " czk." << std::endl;

    delete ucto;
*/
    return 0;
}
