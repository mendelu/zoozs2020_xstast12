#include <iostream>
using namespace std;

class Prestupek{
    string m_popis;
    int m_body;

public:
    Prestupek(string popis, int body){
        m_popis = popis;
        m_body = body;
    }
    string getPopis(){
        return m_popis;
    }

    int getBody(){
        return m_body;
    }

};

class Ridic{
    string m_meno;
    int m_body;
    string m_zaznam;

public:
    Ridic(string jmeno){
        m_meno = jmeno;
        m_body = 10;
        m_zaznam = "";
    }

    Ridic(string jmeno, int body, string zaznam){
        m_meno = jmeno;
        m_body = body;
        m_zaznam = zaznam;
    }

    string getJmeno(){
        return m_meno;
    }

    string getZaznam(){
        return m_zaznam;
    }

    void zaevidujPrestupek(Prestupek* prestupek){
        m_zaznam += prestupek->getPopis();
        m_body -= prestupek->getBody();
        if (m_body < 1){
            cout << "Ridici: " << getJmeno() << ", bol odobraty ridicak!" << endl;
        }
    }

    void print(){
        cout << "Ridic: " << getJmeno() << ", Prestupky: " << getZaznam() <<endl;

    }
};



int main() {
    Ridic *Nikolas = new Ridic("Nikolas Stastny");
    Ridic *David = new Ridic("David Prochazka");

    Prestupek* rychlostMala = new Prestupek("Rychlost prekrocena o 10", 3);
    Prestupek* rychlostStredna = new Prestupek("Rychlost prekrocena o 20", 4);
    Prestupek* rychlostVelka = new Prestupek("Rychlost prekrocena o 30", 5);
    Prestupek* cervena = new Prestupek("Jazda na cervenu", 6);

    Nikolas->zaevidujPrestupek(cervena);
    Nikolas->zaevidujPrestupek(rychlostStredna);
    Nikolas->print();



    delete Nikolas;
    delete David;
    delete rychlostMala;
    delete rychlostStredna;
    delete rychlostVelka;
    delete cervena;
    return 0;
}
