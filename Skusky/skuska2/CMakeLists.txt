cmake_minimum_required(VERSION 3.17)
project(skuska2)

set(CMAKE_CXX_STANDARD 14)

add_executable(skuska2 main.cpp Rostlina.cpp Rostlina.h Strom.cpp Strom.h Kvetina.cpp Kvetina.h Zahrada.cpp Zahrada.h)