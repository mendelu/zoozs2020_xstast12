//
// Created by Nick on 30. 11. 2020.
//

#ifndef SKUSKA2_KVETINA_H
#define SKUSKA2_KVETINA_H

#include "Rostlina.h"

class Kvetina: public Rostlina {
    int m_rgb[3];
public:
    Kvetina(int pc, std::string nazov, int red, int green, int blue);
    void vypisRostlinu();
    int getNakladyPorizeni();

};


#endif //SKUSKA2_KVETINA_H
