//
// Created by Nick on 30. 11. 2020.
//

#include "Zahrada.h"

Zahrada::Zahrada(int rozpocet) {m_rozpocet = rozpocet;}

void Zahrada::pridajRostlinu(Rostlina *rostlina) {
    int cenaRostliny = rostlina->getNakladyPorizeni();
    if (m_rozpocet >= cenaRostliny){
        m_pestovaneRastliny.push_back(rostlina);
        m_rozpocet -= cenaRostliny;
    }
}

void Zahrada::printInfo() {
    std::cout << "Stav rozpoctu: " << m_rozpocet << std::endl;
    std::cout << "Na zahrade su tieto rastliny:" << std::endl;
    for (auto rastlina:m_pestovaneRastliny) {
        rastlina->vypisRostlinu();
        std::cout<<"---------------------" << std::endl;
    }
}

Zahrada::~Zahrada() {
    for (auto rastlina:m_pestovaneRastliny) {
        delete rastlina;
    }
}