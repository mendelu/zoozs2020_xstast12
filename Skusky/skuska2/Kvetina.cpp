//
// Created by Nick on 30. 11. 2020.
//

#include "Kvetina.h"

Kvetina::Kvetina(int pc, std::string nazov, int red, int green, int blue):Rostlina(pc, nazov) {
    m_rgb[0] = red;
    m_rgb[1] = green;
    m_rgb[2] = blue;
}

void Kvetina::vypisRostlinu() {
    std::cout << "Kvetina s nazvom: " << m_nazev << ", pc = "
              << m_porizovaciCena << ", a RGB: " <<
              "(" << m_rgb[0] << ", " << m_rgb[1]<< ", " << m_rgb[2] << ")"   <<std::endl;
}

int Kvetina::getNakladyPorizeni() {
    return m_porizovaciCena+(m_rgb[0]*10+m_rgb[1]*1+m_rgb[2]*10);
}
