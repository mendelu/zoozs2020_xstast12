//
// Created by Nick on 30. 11. 2020.
//

#ifndef SKUSKA2_ROSTLINA_H
#define SKUSKA2_ROSTLINA_H

#include <iostream>

class Rostlina {
protected:
    int m_porizovaciCena;
    std::string m_nazev;
public:
    Rostlina(int pc, std::string nazev);
    virtual void vypisRostlinu()=0;
    virtual int getNakladyPorizeni()=0;
};


#endif //SKUSKA2_ROSTLINA_H
