//
// Created by Nick on 30. 11. 2020.
//

#ifndef SKUSKA2_STROM_H
#define SKUSKA2_STROM_H

#include "Rostlina.h"

class Strom: public Rostlina {
    int m_stariStromu;
public:
    Strom(int pc, std::string nazev, int stariStromu);
    void vypisRostlinu();
    int getNakladyPorizeni();
};


#endif //SKUSKA2_STROM_H
