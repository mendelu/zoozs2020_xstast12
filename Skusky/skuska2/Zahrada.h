//
// Created by Nick on 30. 11. 2020.
//

#ifndef SKUSKA2_ZAHRADA_H
#define SKUSKA2_ZAHRADA_H

#include <vector>
#include "Rostlina.h"

class Zahrada {
    int m_rozpocet;
    std::vector<Rostlina*> m_pestovaneRastliny;
public:
    Zahrada(int rozpocet);
    void pridajRostlinu(Rostlina* rostlina);
    void printInfo();
    ~Zahrada();
};


#endif //SKUSKA2_ZAHRADA_H
