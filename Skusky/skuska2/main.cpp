#include <iostream>
#include "Strom.h"
#include "Kvetina.h"
#include "Zahrada.h"


int main() {
    Zahrada* zahrada = new Zahrada(1000);
    Rostlina* dub = new Strom(30, "Dub", 3);
    Rostlina* jedla = new Strom(10, "Jedla", 1);
    Rostlina* ruza = new Kvetina(80, "ruza", 3,5,6);
    Rostlina* tulipan = new Kvetina(80, "tulipan", 7,25,6);

    zahrada->pridajRostlinu(dub);
    zahrada->pridajRostlinu(jedla);
    zahrada->pridajRostlinu(ruza);
    zahrada->pridajRostlinu(tulipan);

    zahrada->printInfo();

    delete zahrada;
    return 0;
}
