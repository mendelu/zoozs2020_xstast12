//
// Created by Nick on 30. 11. 2020.
//

#include "Strom.h"

Strom::Strom(int pc, std::string nazev, int stariStromu):Rostlina(pc, nazev){
    m_stariStromu =stariStromu;
}

int Strom::getNakladyPorizeni() {
    return m_porizovaciCena+50*m_stariStromu;
}

void Strom::vypisRostlinu() {
    std::cout << "Strom s nazvom: " << m_nazev << ", pc = "
    << m_porizovaciCena << ", a vekom: " << m_stariStromu <<std::endl;
}