#include <array>
#include <iostream>

using namespace std;

class Car {

    class Motor {
        int m_power;
    public:
        Motor(int power) {
            m_power = power;
        }

        int getPower() {
            return m_power;
        }
    };

    class Tire {
        float m_pressure;
    public:
        Tire(float pressure) {
            m_pressure = pressure;
        }

        void setPressure(float press) {
            m_pressure = press;
        }

        float getPress() {
            return m_pressure;
        }
    };

    string m_color;
    Motor *m_motor;
    array<float, 4> m_tirePressure;
    array<Tire *, 4> m_tires;

public:
    Car(string color, int motorPower, array<float, 4> pressure) {
        m_color = color;
        m_motor = new Motor(motorPower);
        for (int i = 0; i < 4; i++) {
            m_tires.at(i) = new Tire(pressure.at(i));
        }
    }

    void setTirePressure(int whichTire, float pressure) {
        m_tirePressure.at(whichTire) = pressure;
    }

    float getTirePressure(int which) {
        return m_tirePressure.at(which);
    }

    ~Car() {
        delete m_motor;
        for (int i = 0; i < 4; i++) {
            delete m_tires.at(i);
        }
    }

    void print() {
        cout << "Color: " << m_color << endl;
        cout << "Engine Power: " << m_motor->getPower() << " KW" << endl;
        cout << "TirePressure predne[0]: " << m_tires.at(0)->getPress() << endl;
        cout << "TirePressure zadne[2]: " << m_tires.at(2)->getPress() << endl;
    }
};

int main() {
    Car *fiat = new Car("Red", 59, {2.3, 2.3, 2.0, 2.0});
    fiat->print();

//    array<int, 5> myArray = {50,10,20,30,40};
//    cout << myArray.at(3) << endl;
//    cout << "size: " << myArray.size() << endl;

    delete fiat;
    return 0;
}
