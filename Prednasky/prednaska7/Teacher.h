/**
 * @brief Reprezentuje triedu ucitela
 *
 * @bug neosterujem nic
 */

#ifndef PREDNASKA7_TEACHER_H
#define PREDNASKA7_TEACHER_H

#include <vector>
#include "Course.h"

namespace uis {
    class Teacher {
        std::vector<Course *> m_courses;
    public:
        Teacher();

        void addCourse(Course *newCourse);

        void printCourses();
    };
}

#endif //PREDNASKA7_TEACHER_H
