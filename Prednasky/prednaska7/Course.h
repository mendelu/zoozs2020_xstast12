/**
 * @brief Reprezentuje triedu kurz
 *
 * @bug neosterujem nic
 */

#ifndef PREDNASKA7_COURSE_H
#define PREDNASKA7_COURSE_H

#include <iostream>

namespace uis{
    class Course {
        std::string m_title; ///< Nazov kurzu
        int m_credits; ///< pocet kreditov
    public:
        /**
         * @brief konstruktor kurzu vynucujici jmeno a kredity
         *
         * konstruktor nastavi kredity a jmeno na predane hodnoty.
         * Dale...
         *
         * @param title  naze kurzu
         * @param credits pocet kreditov za absolvovanie
         */
        Course(std::string title, int credits);
        void printInfo();
    };

}



#endif //PREDNASKA7_COURSE_H
