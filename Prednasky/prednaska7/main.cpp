#include <iostream>
#include "Course.h"
#include "Teacher.h"

using namespace uis;

int main() {
    Course* zoo = new Course("Zoo", 5);
    Course* tzi = new Course("tzi", 6);
//    zoo->printInfo();

    Teacher *nikolas = new Teacher();
    nikolas->addCourse(zoo);
    nikolas->addCourse(tzi);
    nikolas->printCourses();

    delete zoo;
    delete tzi;
    delete nikolas;
    return 0;
}
