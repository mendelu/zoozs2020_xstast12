//
// Created by Nick on 2. 11. 2020.
//

#include "Course.h"

uis::Course::Course(std::string title, int credits){
    m_title = title;
    m_credits = credits;
}

void uis::Course::printInfo(){
    std::cout << "Title: " << m_title << std::endl;
    std::cout << "Credits: " << m_credits << std::endl;
}
