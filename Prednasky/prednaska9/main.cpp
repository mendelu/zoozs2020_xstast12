#include <iostream>

//abstraktna trieda
class GrafickyObjekt{
protected:
    int m_stredX;
    int m_stredY;

public:
    GrafickyObjekt(int stredX,int stredY){
        m_stredX = stredX;
        m_stredY = stredY;
    }

    //cisto virtualne metody
    virtual float getObvod() = 0;
    virtual float getObsah() = 0;

    virtual void kdoJsem(){
        std::cout << "Jsem GrafickyObjekt" << std::endl;
    }

    virtual ~GrafickyObjekt(){
        std::cout << "Konci grafickyObjekt" << std::endl;
    }
};


class Stvorec: public GrafickyObjekt{
private:
    float m_velkostStrany;
public:
    Stvorec(int stredX, int stredY, float velkostStrany):GrafickyObjekt(stredX, stredY){
        m_velkostStrany = velkostStrany;
    }

    float getObvod(){
        return 4*m_velkostStrany;
    }

    float getObsah(){
        return m_velkostStrany*m_velkostStrany;
    }

    void kdoJsem(){
        std::cout << "Jsem Stvorec" << std::endl;
    }

    //len u stvtorca
    void pozdrav(){
        std::cout << "ahoj, jsem ctverec" << std::endl;
    }

    ~Stvorec(){
        std::cout << "Konci Stvorec" << std::endl;
    }
};

class Kruh: public GrafickyObjekt{
private:
    float m_polomer;
    constexpr static float s_pi = 3.14;
public:
    Kruh(int stredX, int stredY, float polomer):GrafickyObjekt(stredX, stredY) {
        m_polomer = polomer;
    }

    float getObvod(){
        return 2*s_pi*m_polomer;
    }

    float getObsah(){
        return s_pi*m_polomer*m_polomer;
    }

    ~Kruh(){
        std::cout << "Konci Kruh" << std::endl;
    }
};

void vypocitaj(GrafickyObjekt *objekt, std::string input){
    if(input =="c"){
        objekt = new Stvorec(10,10,10);
    } else{
        objekt = new Kruh(10,10, 5);
    }
    objekt-> kdoJsem();
    std::cout << "Obvod : " << objekt->getObvod() << std::endl;
    std::cout << "Obsah : " << objekt->getObsah() << std::endl;

}


int main() {
    std::cout << "Uzivaateli vyber zda chces [c]tverec nebo [k]ruh :";
    std::string input = "";
    std::cin >> input;

    GrafickyObjekt* objekt = nullptr;
    if(input =="c"){
        objekt = new Stvorec(10,10,10);
    } else{
        objekt = new Kruh(10,10, 5);
    }
    objekt-> kdoJsem();
    std::cout << "Obvod : " << objekt->getObvod() << std::endl;
    std::cout << "Obsah : " << objekt->getObsah() << std::endl;


//    vypocitaj(objekt, input);




    delete objekt;
    return 0;
}
