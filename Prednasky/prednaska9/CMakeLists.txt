cmake_minimum_required(VERSION 3.17)
project(prednaska9)

set(CMAKE_CXX_STANDARD 14)

add_executable(prednaska9 main.cpp)