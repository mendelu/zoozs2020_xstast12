#include <iostream>
using namespace std;

class Brneni{
    int m_bonusObrany;

public:
    Brneni(int bonusObrany){
        m_bonusObrany = bonusObrany;
    }

    int getBonuObrany(){
        return m_bonusObrany;
    }
};


class Rytir{
    int m_obrana;
    Brneni *m_neseneBrneni;
public:
    Rytir(int obrana){
        m_obrana = obrana;
        m_neseneBrneni = nullptr;
    }

    void seberBrneni(Brneni *brneni){
        if (m_neseneBrneni == nullptr){
            m_neseneBrneni = brneni;
        }
        else{
            cout << "Uz jedno mas. Najprv zahod brnenie" << endl;
        }

    }

    void zahodBrneni(){
        m_neseneBrneni = nullptr;
    }

    int getObrana(){
        if (m_neseneBrneni == nullptr){
            return m_obrana;
        }
        return m_obrana + m_neseneBrneni->getBonuObrany();
    }

};



int main() {
    Rytir *artus = new Rytir(50);
    Brneni *platove = new Brneni(20);
    Brneni *draci = new Brneni(40);


    artus->seberBrneni(platove);
    artus->seberBrneni(draci);
    cout << "Obrana s brnenim: " << artus->getObrana() << endl;
    artus->zahodBrneni();
    cout << "Obrana bez brnenina: " << artus->getObrana();


    delete platove;
    delete artus;
    return 0;
}
