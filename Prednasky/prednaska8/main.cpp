#include <iostream>

class Zamestnanec{
protected:
    std::string m_jmeno;

public:
    Zamestnanec(){
        m_jmeno = "Jmeno nezname";
    }

    std::string getJmeno(){
        return m_jmeno;
    }

    void setJmeno(std::string noveJmeno){
        m_jmeno = noveJmeno;
    }
};

class Ucitel:public Zamestnanec{
    std::string m_obor;

public:
    Ucitel(){
        m_obor = "Obor neznany";
    }

    void printInfo(){
        std::cout << "Obor: " << m_obor << std::endl;
        std::cout << "Jmeno: " << m_jmeno << std::endl;
        //std::cout << "Jmeno: " << getJmeno() << std::endl;
    }
};

int main() {
    Ucitel* david = new Ucitel();
    david->setJmeno("David");
    david->printInfo();
    //david->m_jmeno = "Petr"; // ani v pripade protected



    delete david;
    return 0;
}
