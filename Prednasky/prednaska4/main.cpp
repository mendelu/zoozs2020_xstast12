#include <iostream>
using namespace std;

class UserSettings{
    static string s_playerNick;
    static int s_score;


    public:
        static void changeUserNick(string nick){
            s_playerNick = nick;
        }

        static string getUserNick(){
            return s_playerNick;
        }

        static void addScore(int newScore){
            s_score += newScore;
        }

        static int getScore(){
            return s_score;
        }

};

class Jidlo{
    int m_zivoty;
public:
    Jidlo(int zivoty){
        m_zivoty = zivoty;
    }

    int getZivoty(){
        return m_zivoty;
    }
};

class Prisera{
    int m_zivoty;
    int m_sila;

public:
    Prisera(int sila){
        m_sila = sila;
        m_zivoty = 100;
    }

    int getSila(){
        return m_sila;
    }

    void branSe(int silaProtivnika){
        m_zivoty -= silaProtivnika;
    }

    void branSe(Prisera* utociciPrisera){
        m_zivoty -= utociciPrisera->getSila();
    }

    int getZivoty(){
        return m_zivoty;
    }

    void snez(Jidlo* jidlo){
        m_zivoty += jidlo->getZivoty();
    }

};





int UserSettings::s_score = 0;
string UserSettings::s_playerNick = "No name";

int main() {

    UserSettings::addScore(10);
    UserSettings::addScore(30);

    UserSettings::changeUserNick("Nikolas");
    UserSettings::addScore(50);

//    cout << "pocet bodov co ma " << UserSettings::getUserNick() <<" je " << UserSettings::getScore() << endl;

    Prisera* drak = new Prisera(80);
    Prisera* pes = new Prisera(30);
    drak->branSe(pes);


    cout << drak->getZivoty() << endl;

    Jidlo* ovce = new Jidlo(20);
    drak->snez(ovce);
    cout << drak->getZivoty();

    delete ovce;
    delete drak;
    delete pes;


    return 0;
}
