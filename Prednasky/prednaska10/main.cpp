#include <iostream>

class Mutant{
  int m_sila;
// odebrano public
  Mutant(int sila){
    m_sila = sila;
  }

public:
  int getSila(){
    return m_sila;
  }

  static Mutant* createMutant(int obtiznost){
    Mutant* nepritel = nullptr;

    if (obtiznost == 0){
      nepritel = new Mutant(10);
    } else if (obtiznost == 1) {
      nepritel = new Mutant(15);
    } else {
      nepritel = new Mutant(25);
    }

    return nepritel;
  }
};

int main() {
  // dotaz na obtiznost
  int obtiznost = 1; // 0 = lehka, 1 = streni, 2 = obtizna

  // jak se vyhnout tomu, abych toto nemusel opakovat v systemu?
  Mutant* nepritel = nullptr;
  if (obtiznost == 0){
    nepritel = new Mutant(10);
  } else if (obtiznost == 1) {
    nepritel = new Mutant(15);
  } else {
    nepritel = new Mutant(30);
  }

  Mutant* nepritel2 = Mutant::createMutant(obtiznost);

  return 0;
}